<nav class="" id="main-nav">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <ul>
                    <!-- <li><a id ="historical-home" class="historical" data-historical-delay="0" href="<?php echo 'https://shoringengineers.com/'; ?>">home</span></a></li> -->
                    <li>
                        <div id="historical-expertise" class="large-font has-submenu" data-historical-delay="0" href="">
                            <span class="hidden-xs">areas of</span> expertise <i class="fa fa-angle-double-right"></i>
                            <ul class="inner-expanded">
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-expertise-project" href="<?php echo 'https://shoringengineers.com/'.'expertise/project-management'; ?>">project management</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-expertise-markets" href="<?php echo 'https://shoringengineers.com/'.'expertise/markets'; ?>">markets</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-expertise-shoring" href="<?php echo 'https://shoringengineers.com/'.'expertise/shoring'; ?>">shoring</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-expertise-excavation" href="<?php echo 'https://shoringengineers.com/'.'expertise/excavation'; ?>">excavation</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-expertise-caissons" href="<?php echo 'https://shoringengineers.com/'.'expertise/caissons'; ?>">caissons</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-expertise-shotcrete" href="<?php echo 'https://shoringengineers.com/'.'expertise/shotcrete'; ?>">shotcrete</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <div class="large-font has-submenu">about shoring <i class="fa fa-angle-double-right"></i>
                            <ul class="inner-expanded">
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-about" href="<?php echo 'https://shoringengineers.com/'.'about/history'; ?>">history</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-about-mission" href="<?php echo 'https://shoringengineers.com/'.'about/mission'; ?>">mission</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-about-management" href="<?php echo 'https://shoringengineers.com/'.'about/management'; ?>">management</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-about-community" href="<?php echo 'https://shoringengineers.com/'.'about/community-involvement'; ?>">community involvement</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-about-testimonials" href="<?php echo 'https://shoringengineers.com/'.'about/testimonials'; ?>">testimonials</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="jason-weinstein" href="<?php echo 'https://shoringengineers.com/'.'about/jason-weinstein'; ?>">jason weinstein</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-about-careers" href="<?php echo 'https://shoringengineers.com/'.'about/careers'; ?>">careers</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="hidden-xs">
                        <a id="historical-50th" class="historical large-font" data-historical-delay="0" href="<?php echo 'https://shoringengineers.com/'.'50th'; ?>">Timeline - 50th Anniversary</a>
                    </li>
                    <li>
                        <div class="large-font has-submenu">resources <i class="fa fa-angle-double-right"></i>
                            <ul class="inner-expanded">
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-resources-people" href="<?php echo 'https://shoringengineers.com/'.'resources/people'; ?>">people</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-resources-equipment" href="<?php echo 'https://shoringengineers.com/'.'resources/equipment'; ?>">equipment</a>
                                </li>
                                <li>
                                    <a data-historical-delay="0" class="historical" id="historical-resources-tooling" href="<?php echo 'https://shoringengineers.com/'.'resources/tooling'; ?>">tooling</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <!--<li><a id ="historical-expertise" class="historical large-font" data-historical-delay="0" href="<?php echo 'https://shoringengineers.com/'.'expertise'; ?>">expertise <span class="fa fa-angle-double-right"></a></li> -->
                    <li>
                        <a id="historical-gallery" class="historical large-font" data-historical-delay="0" href="<?php echo 'https://shoringengineers.com/'.'gallery'; ?>">project gallery</a>
                    </li>
                    <li class="hidden-xs">
                        <a id="historical-map" class="large-font" href="<?php echo 'https://shoringengineers.com/'.'map'; ?>" data-historical-delay="0">project map</a>
                    </li>
                    <li>
                        <a id="historical-safety" class="historical large-font" data-historical-delay="0" href="<?php echo 'https://shoringengineers.com/'.'safety'; ?>">safety</a>
                    </li>
                    <li>
                        <a id="historical-contact" class="historical large-font" data-historical-delay="0" href="<?php echo 'https://shoringengineers.com/'.'contact'; ?>">contact</a>
                    </li>
                </ul>
                <span class="hidden-xs"><br><br><br></span>
            </div>
            <div class="col-xs-12">
                <ul>
                    <li class="social-menu-section">
                        <a href="https://www.facebook.com/shoringengineers/?fref=ts" target="_blank" class="mar-r-0-5"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.linkedin.com/company/shoring-engineers?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A2918649%2Cidx%3A2-3-4%2CtarId%3A1448916989750%2Ctas%3Ashorin" target="_blank"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<nav class="helvetica hidden-xs" id="main-nav-white">
    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <ul>
                    <li>
                        <a class="historical twenty-one-hundred unwow" data-wow-animation="fadeInDown" data-wow-delay="500ms" data-unwow-animation="fadeOutUp" data-unwow-delay="0ms" data-historical-delay="900" href="<?php echo 'https://shoringengineers.com/'.'about/history'; ?>">about</a>
                    </li>
                    <li>
                        <a class="historical twenty-one-hundred unwow" data-wow-animation="fadeInDown" data-wow-delay="400ms" data-unwow-animation="fadeOutUp" data-unwow-delay="100ms" data-historical-delay="900" href="<?php echo 'https://shoringengineers.com/'.'resources/people'; ?>">resources</a>
                    </li>
                    <li>
                        <a class="historical twenty-one-hundred unwow" data-wow-animation="fadeInDown" data-wow-delay="300ms" data-unwow-animation="fadeOutUp" data-unwow-delay="200ms" data-historical-delay="900" href="<?php echo 'https://shoringengineers.com/'.'expertise/project-management'; ?>">expertise</a>
                    </li>
                    <li>
                        <a class="historical twenty-one-hundred unwow" data-wow-animation="fadeInDown" data-wow-delay="200ms" data-unwow-animation="fadeOutUp" data-unwow-delay="300ms" data-historical-delay="900" href="<?php echo 'https://shoringengineers.com/'.'safety'; ?>">safety</a>
                    </li>
                    <li>
                        <a class="historical twenty-one-hundred unwow" data-wow-animation="fadeInDown" data-wow-delay="100ms" data-unwow-animation="fadeOutUp" data-unwow-delay="400ms" data-historical-delay="900" href="<?php echo 'https://shoringengineers.com/'.'gallery'; ?>">gallery</a>
                    </li>
                    <li>
                        <a class="historical twenty-one-hundred unwow" data-wow-animation="fadeInDown" data-wow-delay="100ms" data-unwow-animation="fadeOutUp" data-unwow-delay="400ms" data-historical-delay="900" href="<?php echo 'https://shoringengineers.com/'.'50th'; ?>">Timeline</a>
                    </li>
                    <li>
                        <a class=" twenty-one-hundred unwow" data-wow-animation="fadeInDown" data-wow-delay="100ms" data-unwow-animation="fadeOutUp" data-unwow-delay="400ms" data-historical-delay="900" href="<?php echo 'https://shoringengineers.com/'.'map'; ?>">map</a>
                    </li>
                    <li>
                        <a class="historical twenty-one-hundred unwow" data-wow-animation="fadeInDown" data-wow-delay="0ms" data-unwow-animation="fadeOutUp" data-unwow-delay="500ms" href="<?php echo 'https://shoringengineers.com/'.'contact'; ?>" data-historical-delay="900">contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
