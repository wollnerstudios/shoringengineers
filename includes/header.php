<header id="main-header" class="hidden-xs">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="brand-box <?php echo parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) === '/' ? '' : 'show-green-logo' ?>">
                    <div>
                        <img class="toggle-image-nav img-responsive white-logo" src="<?php echo 'https://shoringengineers.com/assets/images/'.'Shoring-logo-white.png'; ?>" alt="">
                    </div>
                    <div class="green-nav">
                        <div class="green-nav-logo">
                            <a class="" id="historical-home" data-historical-delay="0" href="<?php echo 'https://shoringengineers.com/'; ?>">
                                <img class="toggle-image-nav img-responsive green-logo" src="<?php echo 'https://shoringengineers.com/assets/images/'.'green-alpha-logo.png'; ?>" alt="">
                            </a>
                        </div>
                        <div class="green-nav-box">
                            <i id="icon"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-72476418-1', 'auto');
    ga('send', 'pageview');

    </script>
</header>
<header id="mobile-header" class="visible-xs">
    <div class="table-fluid">
        <div class="table-row text-center">
            <div class="table-cell col-xs-3 brand-section">
                <a href="<?php echo 'https://shoringengineers.com/'; ?>" class="historical">
                    <img src="<?php echo 'https://shoringengineers.com/assets/images/'.'green-alpha-logo.png'; ?>" alt="">
                </a>
            </div>
            <div class="table-cell col-xs-3 icon-section ">
                <i id="icon-mobile"></i>
            </div>
            <div class="table-cell col-xs-3 phone-section">
                <a href="tel:+1-562-944-9331">
                    <i class="fa fa-phone"></i>
                </a>
            </div>
            <div class="table-cell col-xs-3 contact-section">
                <a href="<?php echo 'https://shoringengineers.com/'.'contact'; ?>" class="historical">
                    <i class="fa fa-envelope"></i>
                </a>
            </div>
        </div>
    </div>

</header>