<link rel="icon" href="<?php echo 'https://shoringengineers.com/'.'favicon.ico'; ?>">

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

<link href="<?php echo VENDORPATH.'bootstrap/dist/css/bootstrap.min.css'; ?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo VENDORPATH.'wow/css/libs/animate.css'; ?>">
<link rel="stylesheet" href="<?php echo VENDORPATH.'font-awesome/css/font-awesome.css'; ?>">
<link rel="stylesheet" href="<?php echo VENDORPATH.'marka/dist/css/marka.min.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo VENDORPATH.'revolution/css/settings.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo VENDORPATH.'revolution/css/layers.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo VENDORPATH.'revolution/css/navigation.css'; ?>">

<link href='https://api.mapbox.com/mapbox.js/v2.2.2/mapbox.css' rel='stylesheet'/>
<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css' rel='stylesheet'/>
<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css' rel='stylesheet'/>

<link rel="stylesheet" href="<?php echo VENDORPATH.'magnific/dist/magnific-popup.css'; ?>">
<link rel="stylesheet" href="<?php echo VENDORPATH.'fancybox/source/jquery.fancybox.css?v=2.1.5'; ?>" type="text/css" media="screen"/>
<link rel="stylesheet" href="<?php echo VENDORPATH.'fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7'; ?>" type="text/css" media="screen"/>

<link rel="stylesheet" href="<?php echo CUSTOMPATH.'material-form.css'; ?>">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css"/>

<link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom-form.css'; ?>">
<link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom.css'; ?>">
<link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom-md.css'; ?>">
<link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom-sm.css'; ?>">
<link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom-xs.css'; ?>">
<link rel="stylesheet" href="<?php echo CUSTOMPATH.'mapbox.css'; ?>">
<link rel="stylesheet" href="<?php echo CUSTOMPATH.'management.css'; ?>">
<link rel="stylesheet" href="<?php echo CUSTOMPATH.'testimonials.css'; ?>">
<link rel="stylesheet" href="<?php echo CUSTOMPATH.'subpages.css'; ?>">
<link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom-style.css'; ?>">
<link rel="stylesheet" href="<?php echo VENDORPATH.'lazyloadxt/jquery.lazyloadxt.spinner.css'; ?>">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
