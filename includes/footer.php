<footer class="pad-t-2 pad-b-2" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-xs-6">
                <img class="img-responsive" src="<?php echo 'https://shoringengineers.com/assets/images/'.'Shoring-logo-bottom.png'; ?>" alt="">
            </div>
            <div class="col-xs-3 hidden-xs" id="footer-address-block">
                <address>
                    Shoring Engineers <br>
                    12645 Clark Street <br>
                    Santa Fe Springs <br>
                    CA 90670 <br>
                    Phone: <a href="tel:+1-562-944-9331">(562) 944-9331</a> <br>
                    Fax: <a href="tel:+1-562-941-8098">(562) 941-8098</a>
                </address>
            </div>
            <div class="col-sm-4 col-xs-6" id="footer-license-block">
                CA Lic. #245416-A <br>
                AZ Lic. #ROC124501<br>
                NV Lic. #0055754 <br>
                OR Lic. #188382 <br>
                UT Lic. #312403-5501 <br>
                WA Lic. #2610<br>
                <span class="hidden-xs"><br><br></span>
                <span class="hidden-xs">Website Development by
                    <a target="_blank" href="http://www.wollnerstudios.com">W Brand Studio</a></span>
            </div>
            <div class="visible-xs">
                <div class="col-xs-12">
                    <address>

                        12645 Clark Street
                        Santa Fe Springs
                        CA 90670 <br>
                        Phone: <a href="tel:+1-562-944-9331">(562) 944-9331</a>
                        Fax: <a href="tel:+1-562-941-8098">(562) 941-8098</a>
                    </address>
                </div>
            </div>
            <div class="col-sm-3 hidden-xs">
                <a target="_blank" href="https://www.linkedin.com/company/2918649?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A2918649%2Cidx%3A2-3-4%2CtarId%3A1448916989750%2Ctas%3Ashorin" class="pull-right pad-l-2">
                    <img class="img-responsive pull-right" src="<?php echo 'https://shoringengineers.com/assets/images/'.'linkedIn-icon.png'; ?>" </a>
                <a target="_blank" href="https://www.facebook.com/shoringengineers/?fref=ts" class="pull-right ">
                    <img class="img-responsive pull-right" src="<?php echo 'https://shoringengineers.com/assets/images/'.'facebook-icon.png'; ?>"</div>
            <div class="visible-xs mobile-social">
                <div class="col-xs-12">
                    <a target="_blank" href="https://www.facebook.com/shoringengineers/?fref=ts" class=" ">
                        <img class="img-responsive " src="<?php echo 'https://shoringengineers.com/assets/images/'.'facebook-icon.png'; ?>" alt="">
                    </a>
                    <a target="_blank" href="https://www.linkedin.com/company/2918649?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A2918649%2Cidx%3A2-3-4%2CtarId%3A1448916989750%2Ctas%3Ashorin" class=" pad-l-2">
                        <img class="img-responsive " src="<?php echo 'https://shoringengineers.com/assets/images/'.'linkedIn-icon.png'; ?>" alt="">
                    </a>
                    <div id='15312'><a href="sitemap.xml">Sitemap</a></div>
                    <script>(function (g, h, i, f) {
                        i.getElementById(f).style['dis' + g] = 'n' + h;
                    })('play', 'one', document, 85 * 179 + 97);</script>
                </div>
            </div>
        </div>
    </div>
</footer>