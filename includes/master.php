<!DOCTYPE html>
<?php echo getcwd()."\n"; ?>
<html lang="en" class="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="description"/>
        <title>Shoring Engineers</title>
        <meta name="keywords" content="keywords"/>
        <meta name="author" content="WollnerStudios, Inc">
        <meta name="robots" content="all"/>
        <meta name="robots" content="index, follow"/>
        <meta name="revisit-after" content="4 days"/>

        <?php include('head.php'); ?>
    </head>
    <body>
        <div id="page-wrap">
            <div id="page">
                <?php include('head.php'); ?>
                <?php include('nav.php'); ?>
                <div class="wrap-for-menu">


                    <?php include('footer.php'); ?>
                </div>
                <div id="menu-overlay"></div>
            </div>
        </div>
        <?php include('scripts.php'); ?>
    </body>
</html>