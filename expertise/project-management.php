<?php require '../config/config.php'; ?>
<!DOCTYPE html>
<html lang="en" class="secondary-page">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="description"/>
        <title>Shoring Engineers</title>
        <meta name="keywords" content="keywords"/>
        <meta name="author" content="WollnerStudios, Inc">
        <meta name="robots" content="all"/>
        <meta name="robots" content="index, follow"/>
        <meta name="revisit-after" content="4 days"/>
        <?php include(INCLUDESPATH.'head.php'); ?>
    </head>
    <body>
        <div id="page-wrap">
            <div id="page">
                <?php include(INCLUDESPATH.'header.php'); ?>
                <?php include(INCLUDESPATH.'nav.php'); ?>
                <div class="wrap-for-menu">

                    <?php include(PAGEPATH.'expertise/project-management.php'); ?>


                    <?php include(INCLUDESPATH.'footer.php'); ?>
                </div>
                <div id="menu-overlay"></div>
            </div>
        </div>
        <?php include(INCLUDESPATH.'scripts.php'); ?>
    </body>
</html>