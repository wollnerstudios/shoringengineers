<?php
require 'config/config.php';
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

    include(PAGEPATH.($path !== '/' ? $path : 'index').'.php');
    die;
}
?>

<!DOCTYPE html>
<html lang="en" class="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Shoring Engineers is over 40 years strong - in a field where strength is everything. We are one of the oldest shoring contractors in California."/>
        <title>Shoring Engineers</title>
        <meta name="keywords" content="keywords"/>
        <meta name="author" content="Shoring Engineers, Inc">
        <meta name="robots" content="all"/>
        <meta name="robots" content="index, follow"/>
        <meta name="revisit-after" content="4 days"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
        $(document).bind('mobileinit', function () {
            $.mobile.changePage.defaults.changeHash = false;
            $.mobile.hashListeningEnabled = false;
            $.mobile.pushStateEnabled = false;
        });
        </script>
        <?php include(INCLUDESPATH.'head.php'); ?>
    </head>
    <body>
        <div id="page-wrap">
            <div id="page">
                <?php include(INCLUDESPATH.'header.php'); ?>
                <?php include(INCLUDESPATH.'nav.php'); ?>
                <div class="wrap-for-menu">
                    <?php
                    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

                    include(PAGEPATH.($path !== '/' ? $path : 'index').'.php');
                    ?>

                    <?php include(INCLUDESPATH.'footer.php'); ?>
                </div>
                <div id="menu-overlay"></div>
            </div>
        </div>
        <?php include(INCLUDESPATH.'scripts.php'); ?>

        <?php if ($path !== '/'): ?>
        <script>
        unWowWhiteNav()
        // setTimeout(function () {
        //     $('.brand-box').addClass('show-green-logo')
        // }, 2100)
        </script>
        <?php endif; ?>
    </body>
</html>