<?php
header('Content-Type: text/html; charset=utf-8');
define('INCLUDESPATH', __DIR__.'/../includes/');
define('PAGEPATH', __DIR__.'/../pages/');
define('URLPATH', 'https://shoringengineers.com/');
define('IMAGEPATH', 'https://shoringengineers.com/assets/images/');
define('VENDORPATH', 'https://shoringengineers.com/assets/vendors/');
define('CUSTOMPATH', 'https://shoringengineers.com/assets/custom/');
