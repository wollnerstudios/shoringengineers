<?php

require 'PHPMailer/PHPMailerAutoload.php';
require '../config/config.php';

/*
name
company
phone
email
address
city
zip
details
request_date
files
*/




// grab post
$request = array(
	'name' => $_POST['name'],
	'company' => $_POST['company'],
	'phone' => $_POST['phone'],
	'email' => $_POST['email'],
	'address' => $_POST['address'],
	'city' => $_POST['city'],
	'zip' => $_POST['zip'],
	'details' => $_POST['details'],
	'request_date' => $_POST['request_date'],
);

function emailShoring($request)
{
	
	// initialize phpmailer
	$mail = new PHPMailer(true);
	$mail->IsHTML(true); 
	$mail->CharSet="utf-8";
	
	//set up email that goes to shoring
	$mail->setFrom('info@shoringengineers.com', 'Shoring Engineers');
	$mail->addAddress('info@shoringengineers.com', 'Shoring Engineers');
	$mail->Subject = 'Request from '.$request['name'].' ['.$request['email'].']';
	
	// replace email template placeholders 
	$body = file_get_contents('email-inlined.html');
	foreach($request as $key => $value){
		$body = str_replace('{{'.$key.'}}', $value, $body);
	}	
	
	$body = str_replace('{{custom_message}}', '', $body);
	$mail->MsgHTML($body);
	
	// check for uploaded files if it exists
	if ($_FILES['files']['tmp_name'])
	{
		$mail->AddAttachment( $_FILES['files']['tmp_name'], $_FILES['files']['name'] );
	}
	
	// send email
	if(!$mail->send())
	{
		header("Location:".'https://shoringengineers.com'."/contact?mail=fail");
		//echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
		
		header("Location:".'https://shoringengineers.com'."/contact?mail=success");
		//echo 'Message has been sent';
	}
}


emailShoring($request);






    



