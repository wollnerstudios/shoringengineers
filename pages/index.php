<section class="historical-container">
    <div class="historical-timing" data-historical-delay="1200"></div>
    <div class="mobile-banner visible-xs" style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'home-mainphoto.jpg'; ?>')">
        <div class="mobile-banner-wrap">
            <img class="img-responsive" src="<?php echo 'https://shoringengineers.com/assets/images/'.'50th-logo-main.png'; ?>" alt="">
            <h1>
                It is with great sadness that we inform you of the passing of Jason Weinstein
            </h1>

            <img src="<?php echo 'https://shoringengineers.com/assets/images/'.'title-divider.png'; ?>" alt="">

            <p style="color: #fff;font-size: 16px;">
                Read George A. Woodley, Jr.'s letter about Jason Weinstein below.
            </p>

            <div class="text-center">
                <div class="Newspaper-Button-2 rev-btn rs-parallaxlevel-0 read-more-button" style="display: inline-block;">
                    <a style="color:#fff;text-decoration:none;" class="historical" href="<?php echo 'https://shoringengineers.com/'.'about/jason-weinstein'; ?>">THE PASSING OF JASON WEINSTEIN</a>
                </div>
            </div>
        </div>
    </div>
    <section id="main-banner" class="hidden-xs">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xs-12 pad-l-0 pad-r-0">
                    <a class="historical hidden-xs" href="<?php echo 'https://shoringengineers.com/'.'50th'; ?>">
                        <img class="fifty-logo-final" src="<?php echo 'https://shoringengineers.com/assets/images/'.'50th-logo-final-white.png'; ?>" alt="">
                    </a>
                    <!-- START REVOLUTION SLIDER 5.0 -->
                    <div class="rev_slider_wrapper">
                        <div id="slider1" class="rev_slider" data-version="5.0">
                            <ul>
                                <li data-transition="fade">
                                    <!-- MAIN IMAGE -->
                                    <div class="slotholder"></div>
                                    <img src="<?php echo 'https://shoringengineers.com/assets/images/'.'home-mainphoto.jpg'; ?>" alt=""
                                         data-bgposition="center center"
                                         data-kenburns="on"
                                         data-duration="30000"
                                         data-ease="Linear.easeNone"
                                         data-scalestart="100"
                                         data-scaleend="120"
                                         data-rotatestart="0"
                                         data-rotateend="0"
                                         data-offsetstart="0 0"
                                         data-offsetend="0 0"
                                         data-bgparallax="10"
                                         class="rev-slidebg"
                                         width="1920"
                                         height="1280"
                                         data-no-retina>
                                    <div class="tp-caption Newspaper-Title-Centered main-slider-header tp-resizeme rs-parallaxlevel-0"
                                         id="slide-140-layer-1"
                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                         data-y="['middle','middle','middle','middle']" data-voffset="['-67','-67,'-67','-47']"
                                         data-fontsize="['50','60','60','50']"
                                         data-lineheight="['52','52','52','32']"
                                         data-width="['800','721','721','420']"
                                         data-height="none"
                                         data-whitespace="normal"
                                         data-transform_idle="o:1;"
                                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                                         data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                         data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                                         data-start="1000"
                                         data-splitin="none"
                                         data-splitout="none"
                                         data-responsive_offset="on"
                                         data-lasttriggerstate="reset"
                                         style="z-index: 7; min-width: 721px; max-width: 721px; white-space: normal;text-align:center;">
                                        It is with great sadness that we inform you of the passing of Jason Weinstein 
                                    </div>
                                    <div class="tp-caption maincaption"
                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                         data-y="['middle','middle','middle','middle']" data-voffset="['5','5','5','5']"
                                         data-fontsize="['50','50','50','30']"
                                         data-lineheight="['52','52','52','32']"
                                         data-width="['850','721','721','420']"
                                         data-height="none"
                                         data-whitespace="normal"
                                         data-transform_idle="o:1;"
                                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                                         data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                         data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                                         data-start="1000"
                                         data-splitin="none"
                                         data-splitout="none"
                                         data-responsive_offset="on"
                                         data-lasttriggerstate="reset"
                                         style=""><img src="<?php echo 'https://shoringengineers.com/assets/images/'.'title-divider.png'; ?>" alt="">
                                    </div>
                                    <div class="tp-caption Newspaper-Title-Centered main-slider-subheader tp-resizeme rs-parallaxlevel-0"
                                         id="slide-140-layer-1"
                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                         data-y="['middle','middle','middle','middle']" data-voffset="['50','50,'50','50']"
                                         data-fontsize="['18','18','18','18']"
                                         data-lineheight="['28','28','28','28']"
                                         data-width="['850','721','721','420']"
                                         data-height="none"
                                         data-whitespace="normal"
                                         data-transform_idle="o:1;"
                                         data-transform_in="y:50px;opacity:0;s:1500;e:Power3.easeInOut;"
                                         data-transform_out="opacity:0;s:2;e:Linear.easeNone;s:2;e:Linear.easeNone;"
                                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                         data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                                         data-start="1000"
                                         data-splitin="none"
                                         data-splitout="none"
                                         data-responsive_offset="on"
                                         data-lasttriggerstate="reset"
                                         style="z-index: 7; min-width: 721px; max-width: 721px; white-space: normal;text-align:center;">
                                        Read George A. Woodley, Jr.'s letter about Jason Weinstein below.
                                    </div>
                                    <div class="tp-caption Newspaper-Button-2 rev-btn rs-parallaxlevel-0 read-more-button"
                                         id="slide-140-layer-4"
                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                         data-y="['middle','middle','middle','middle']" data-voffset="['200','200','200','200']"
                                         data-width="none"
                                         data-height="none"
                                         data-whitespace="nowrap"
                                         data-transform_idle="o:1;"
                                         data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;"
                                         data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                                         data-transform_in="y:50px;opacity:0;s:1500;e:Power3.easeInOut;"
                                         data-transform_out="opacity:0;s:2;e:Linear.easeNone;s:2;e:Linear.easeNone;"
                                         data-start="1000"
                                         data-splitin="none"
                                         data-splitout="none"
                                         data-responsive_offset="on"
                                         data-responsive="on"
                                         data-lasttriggerstate="reset"
                                         style="z-index: 9; white-space: nowrap;">
                                        <a style="color:#fff;text-decoration:none;" class="historical" href="<?php echo 'https://shoringengineers.com/'.'about/jason-weinstein'; ?>">THE PASSING OF JASON WEINSTEIN</a>
                                    </div>
                                </li>
                            </ul>
                        </div><!-- END REVOLUTION SLIDER -->
                    </div><!-- END OF SLIDER WRAPPER -->
                </div>
            </div>
        </div>
    </section>

    <section class="text-center pad-t-2-5 pad-b-2-5" id="main-copy">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <h2>
                        Creating solid foundations that withstand the test of time
                    </h2>
                    <p>
                        For over fifty years Shoring Engineers has provided the backbone to several landmarks stretching from San Francisco to San Diego and beyond. We're one of Southern California's oldest contractors combining the pillars of strength; shoring, caissons, excavation, and shotcrete. From the Cathedral of Our Lady of the Angels to Angel Stadium, private to commercial, educational to government&#8212;we're building toughness and dependability for every project, big or small. It always starts with a firm foundation, that's why all structures should start with Shoring Engineers.</p>

                </div>
            </div>
        </div>
    </section>

    <section class="text-center" id="main-callout">
        <div class="container-fluid">
            <div class="row">
                <div class="callout-box callout-box-1 col-xs-12 col-sm-6 col-md-3 pad-r-0 pad-l-0"
                     style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'shoring-home.jpg'; ?>')">
                    <div class="main-callout-button-container">
                        <div class="main-callout-button">
                            <a class="historical" href="<?php echo 'https://shoringengineers.com/'.'expertise/shoring'; ?>">Shoring +</a></div>
                    </div>
                    <div class="main-callout-placeholder"></div>
                </div>
                <div class="callout-box callout-box-2 col-xs-12 col-sm-6 col-md-3 pad-r-0 pad-l-0"
                     style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'caissons-home-highlight.jpg'; ?>')">
                    <div class="main-callout-button-container">
                        <div class="main-callout-button">
                            <a class="historical" href="<?php echo 'https://shoringengineers.com/'.'expertise/caissons'; ?>">caissons +</a>
                        </div>
                    </div>
                    <div class="main-callout-placeholder"></div>
                </div>
                <div class="callout-box callout-box-3 col-xs-12 col-sm-6 col-md-3 pad-r-0 pad-l-0"
                     style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'excavation-home.jpg'; ?>')">
                    <div class="main-callout-button-container">
                        <div class="main-callout-button">
                            <a class="historical" href="<?php echo 'https://shoringengineers.com/'.'expertise/excavation'; ?>">excavation +</a>
                        </div>
                    </div>
                    <div class="main-callout-placeholder"></div>
                </div>
                <div class="callout-box callout-box-4 col-xs-12 col-sm-6 col-md-3 pad-r-0 pad-l-0"
                     style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'shotcrete-home.jpg'; ?>')">
                    <div class="main-callout-button-container">
                        <div class="main-callout-button">
                            <a class="historical" href="<?php echo 'https://shoringengineers.com/'.'expertise/shotcrete'; ?>">shotcrete +</a>
                        </div>
                    </div>
                    <div class="main-callout-placeholder"></div>
                </div>
            </div>
        </div>
    </section>
</section>