<?php
$slides = [];
$slides[] = [
    'imageFile' => 'assets/images/60s.jpg',
    'slideYear' => '1960s',
    'slideCopy' => 'When it all started.',
    'slideTextBlockImagefile' => 'assets/images/1960s.jpg',
    'slideTextBlockSmallCopy' => '
			Longtime business associates George A. Woodley, drillmaster and Bob Holt, civil engineer, form Shoring Engineers in 1966. ',
    'slideTextBlockLargeCopy' => '
			With only 3 people in the office and 5 field guys, and a Williams ABH drill rig, we were ready to go. Soon after we landed our first project�a hospital on Pico and Beverwill. And so it begins.'
];
$slides[] = [
    'imageFile' => 'assets/images/70s.jpg',
    'slideYear' => '1970s',
    'slideCopy' => 'Here we grow.',
    'slideTextBlockImagefile' => 'assets/images/1970s.jpg',
    'slideTextBlockSmallCopy' => 'With explosive growth, expanding people, all while surviving the recession during the Jimmy Carter era, Shoring Engineers sets up shop in El Monte.',
    'slideTextBlockLargeCopy' => 'On the project front we built foundations for several well-know structures in Los Angeles including the Bonaventure Hotel, Citicorp Building (featured in hit TV show L.A. Law), and Security Pacific Bank. '
];
$slides[] = [
    'imageFile' => 'assets/images/80s.jpg',
    'slideYear' => '1980s',
    'slideCopy' => 'We\'re in the midst of a vigorous expansion phase.',
    'slideTextBlockImagefile' => 'assets/images/1980s.jpg',
    'slideTextBlockSmallCopy' => 'Shoring Engineers continues to get bigger and we move our headquarters further up the road to Durfee Avenue in El Monte. ',
    'slideTextBlockLargeCopy' => 'Bob decides to retire and George becomes sole proprietor. George Jr. comes on board to support his father, as well as civil engineer Jason E. Weinstein and Buster Montank. Our company also expanded its capabilities adding excavation and shotcrete services. '
];
$slides[] = [
    'imageFile' => 'assets/images/90s.jpg',
    'slideYear' => '1990s',
    'slideCopy' => 'We buy a place of our own.',
    'slideTextBlockImagefile' => 'assets/images/1990s.jpg',
    'slideTextBlockSmallCopy' => 'During the nineties, Shoring grew to nearly two hundred employees and so we purchased our own building in Santa Fe Springs, where we still exist today.',
    'slideTextBlockLargeCopy' => 'Original drill operator from day one, Charlie Bardwell retires. And our project portfolio becomes more impressive including the Cathedral of Our Lady of the Angels, Staples Center, retrofit of L.A. City Hall, and our first major out of state job�Phoenix�s Bank of America tower.'
];
$slides[] = [
    'imageFile' => 'assets/images/00s.jpg',
    'slideYear' => '2000s',
    'slideCopy' => 'Our roots are firmly planted.',
    'slideTextBlockImagefile' => 'assets/images/2000s.jpg',
    'slideTextBlockSmallCopy' => 'The 21st century enabled Shoring to switch gears. Two lead operators (whom started as yard hands)�Dwayne Philips and Serafin Gonzales�were promoted to general superintendents. ',
    'slideTextBlockLargeCopy' => 'There was a major expansion in acquiring new drill rigs and excavating equipment. And the reputation of the company was firmly planted, landing such recognizable projects as the Rose Bowl renovation, Dodger Stadium, L.A. Hall of Justice and Apple Headquarters.   '
];
?>

<div class="historical-container">
    <div class="historical-timing" data-historical-delay="2000"></div>
    <div class="white-cover-50">

    </div>
    <div class="fifty-splash-section wow fadeIn">
        <div class="container">
            <div class="row"><br><br><br>
                <div class="col-xs-12 text-center">
                    <img src="<?php echo 'https://shoringengineers.com/assets/images/'.'50th-logo-main.png'; ?>" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 text-center copy-part">
                    <h1>
                        <strong>HALF A CENTURY CONSTRUCTING STRUCTURAL SUPPORT AND WE'RE STILL STANDING TALL.</strong>
                    </h1>
                    <p>
                        Shoring Engineers is proud to acknowledge our 50th anniversary. Since 1966, we've dug thousands of holes, moved mountains of dirt, and erected some of the most famous landmarks, living spaces, and public facilities across Southern California. We've built a reputation for excellence over the years, and we're not about to let anything tarnish it during the next fifty. Thank you to all our faithful employees, loyal contractors, and reliable vendors. Without all of you, we wouldn't be standing tall today.
                    </p>
                    <div class="see-timeline-button mar-t-5">
                        SEE TIMELINE
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="tabled hovering" id="gallerytest">
        <div class="no-hover"></div>
        <div class="copy-section test-copy-description">
            <div class="back-button-50">
                <i class="fa fa-chevron-left"></i>
                <div>Back</div>
            </div>
            <div class="text-copy-cell">
                <div class="text-copy-block animated invisible">
                    <p class="smallish">
                        George A. Woodley, Founder and President George A. Woodley liked drilling from an early age.
                        Married with 5 children, George has enjoyed leading the company for five decades. And he still likes drilling.
                    </p>
                    <img class="copy-img" src="<?php echo 'https://shoringengineers.com/assets/images/'.'1960s.jpg'; ?>" alt="">
                    <p class="largish">
                        Longtime business associates George A. Woodley, drillmaster and Bob Holt, civil engineer, form Shoring Engineers in 1966. With only 3 people in the office and 5 field guys, and a Williams ABH drill rig. Soon after we landed our first project-a hospital on Pico and Beverwill. And so it begins. </p>
                </div>
            </div>
        </div>
        <div class="slide-section">
            <div class="pushing-div"></div>
            <?php $i = 100; ?>
            <?php $j = 1000; ?>
            <?php foreach ($slides as $slide): ?>
                <div
                        data-slide-smallcopy="<?php echo $slide['slideTextBlockSmallCopy']; ?>"
                        data-slide-largeCopy="<?php echo $slide['slideTextBlockLargeCopy']; ?>"
                        data-slide-imgFile="<?php echo $slide['slideTextBlockImagefile']; ?>"
                        class="fiftieth-slide unwow"
                        data-wow-delay="<?php echo $i; ?>ms"
                        data-unwow-delay="<?php echo $j; ?>ms"
                        data-unwow-animation="fadeOutDown"
                        style="background-image:url('<?php echo $slide['imageFile']; ?>')">
                    <div class="slide-text">
                        <div class="slide-year"><span><?php echo $slide['slideYear']; ?></span></div>
                        <div class="slide-button"><i class="fa fa-plus"></i></div>
                        <div class="slide-description">
                            <?php echo $slide['slideCopy']; ?>
                        </div>
                    </div>
                    <div class="blackened"></div>
                </div>
                <?php $i += 100; ?>
                <?php $j -= 100; ?>
            <?php endforeach; ?>

        </div>
    </section>

</div>

