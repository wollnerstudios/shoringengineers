<section class="historical-container">
    <div class="historical-timing" data-historical-delay="2100"></div>
    <div class="container-fluid" style="margin-bottom:2px;">
        <div class="row">
            <div class="page-banner-page col-xs-12 pad-r-0 pad-l-0 wow fadeIn unwow" style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'machinery.jpg'; ?>')" data-unwow-animation="fadeOut" data-unwow-delay="1300ms">

                <div class="page-banner-title">
                    <aside class="col-sm-3 pad-r-0 pad-l-0 ">
                    </aside>
                    <div class="col-sm-3 pad-r-0 dude-dude duder wow fadeIn unwow pad-l-0 " data-unwow-animation="fadeOut" data-unwow-delay="900ms" data-wow-delay="400ms">
                        RESOURCES / equipment
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <aside class="col-sm-3 pad-r-0 pad-l-0 secondary-left-copy-green wow unwow fadeInUp side-menu " data-unwow-animation="fadeOutDown" data-unwow-delay="1100ms" data-wow-delay="200ms">
                <div>
                    <ul>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'resources/people'; ?>">people &nbsp;
                                <span class="i fa fa-angle-double-right"></span></a></li>
                        <li>
                            <a class="active historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'resources/equipment'; ?>">equipment &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'resources/tooling'; ?>">tooling &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                    </ul>
                </div>
            </aside>
            <div class="col-sm-3 pad-r-0 dude-dude pad-l-0 wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="900ms" data-wow-delay="400ms">
                <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'tooling.jpg'; ?>" alt="">
            </div>
            <div class="col-sm-6  pad-r-0 pad-l-0 secondary-right-copy wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="700ms" data-wow-delay="600ms">
                <article>
                    <h2>
                        Better equipment means better results. </h2>
                    <img class="mar-t-2 mar-b-2" src="<?php echo 'https://shoringengineers.com/assets/images/'.'logo-for-2dary-pages-in-text-area.png'; ?>" alt="">
                    <aside>
                        From vibratory drills to tieback drills, low access drills to cranes - we have a massive amount of machinery that is ready to go, no need to locate any rentals.
                    </aside>
                    <p>
                        In fact, we have one of the largest and most up-to-date equipment inventories in the U.S. - enough to cover multiple projects without issue. From open-hole drilling to the fully cased method using oscillators, we have the latest state-of-the-art equipment from Europe. Additionally, we have a huge selection of customized drilling tools as well</p>

                </article>
            </div>
        </div>
    </div>
</section>