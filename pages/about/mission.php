<section class="historical-container">
    <div class="historical-timing" data-historical-delay="2100"></div>
    <div class="container-fluid" style="margin-bottom:2px;">
        <div class="row">
            <div class="page-banner-page col-xs-12 pad-r-0 pad-l-0 wow fadeIn unwow " style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'mission.jpg'; ?>')" data-unwow-animation="fadeOut" data-unwow-delay="1300ms">

                <div class="page-banner-title">
                    <aside class="col-sm-3 pad-r-0 pad-l-0 ">
                    </aside>
                    <div class="col-sm-3 pad-r-0 dude-dude duder wow fadeIn unwow pad-l-0 " data-unwow-animation="fadeOut" data-unwow-delay="900ms" data-wow-delay="400ms">
                        ABOUT / mission
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <aside class="col-sm-3 pad-r-0 pad-l-0 secondary-left-copy-green wow unwow fadeInUp side-menu " data-unwow-animation="fadeOutDown" data-unwow-delay="1100ms" data-wow-delay="200ms">
                <div>
                    <ul>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/history'; ?>">history&nbsp;
                                <span class="i fa fa-angle-double-right"></span></a></li>
                        <li>
                            <a class="active historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/mission'; ?>">mission &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/management'; ?>">management &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/community-involvement'; ?>">community involvement &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/jason-weinstein'; ?>">jason weinstein &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/testimonials'; ?>">testimonials &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/careers'; ?>">careers &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>

                    </ul>

                </div>
            </aside>

            <div class="col-sm-3 pad-r-0 dude-dude pad-l-0 wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="900ms" data-wow-delay="400ms">
                <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'planning.jpg'; ?>" alt="">
            </div>
            <div class="col-sm-6  pad-r-0 pad-l-0 secondary-right-copy wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="700ms" data-wow-delay="600ms">
                <article>
                    <h2>
                        Shoring Engineers is at the forefront of building solid foundations by working smart and safe. </h2>
                    <img class="mar-t-2 mar-b-2" src="<?php echo 'https://shoringengineers.com/assets/images/'.'logo-for-2dary-pages-in-text-area.png'; ?>" alt="">
                    <aside>
                        Our life's work is to ensure that every project - big or small - is well planned out, executed flawlessly, and provides the strength and support needed for years to come.
                    </aside>
                    <p>
                        We possess the expertise and sophisticated capability that is truly essential to create innovative structures, building value for our clients and our own brand as well. However, the real foundation of our success is our people. By ensuring a smart and safe working environment, we're able to deliver continuous and complete satisfaction on every job, every time.
                    </p>

                </article>
            </div>
        </div>
    </div>
</section>
