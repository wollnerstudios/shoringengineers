<section class="historical-container">
    <div class="historical-timing" data-historical-delay="2100"></div>
    <div class="container-fluid" style="margin-bottom:2px;">
        <div class="row">

            <div class="page-banner-page management-banner col-xs-12 pad-r-0 pad-l-0 wow fadeIn unwow" data-unwow-animation="fadeOut" data-unwow-delay="1300ms">

                <div class="management-button-wrapper hidden-xs">
                    <ul class="management-buttons">
                        <li class="manager1 wow fadeIn" data-wow-delay="1500ms">
                            <a href="#manager-1-popup-button " class="fancybox management-button" data-toggle="tooltip" data-placement="top" title="Rene Contreras, P.E., Vice President">
                                <i class="fa fa-plus"></i>
                            </a>
                            <div id="manager-1-popup-button" class="management-popup">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 pad-l-0 pad-r-0">
                                            <div class="popup-copy-container">
                                                <h2>
                                                    Rene Contreras, <br>
                                                    P.E., Vice President
                                                </h2>
                                                <p>
                                                    Rene would truly rather be in the dirt than behind a desk - whether working on a construction site or climbing uphill on his mountain bike. It's all about the great outdoors. And that's why our clients love him. He is our key estimator, but is also deeply rooted in project management and design. After graduating in 1995 from UCI with a B.S. in Civil Engineering, he landed at Shoring in 1996 and hasn't looked back ever since. Rene is also registered in California as a professional engineer. After work, he commits to quality time with his wife and two children.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 pad-l-0 pad-r-0">
                                            <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'rene.jpg'; ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="manager2 wow fadeIn" data-wow-delay="1700ms">
                            <a a href="#manager-2-popup-button" class="management-button fancybox" data-toggle="tooltip" data-placement="top" title="Jason E. Weinstein, P.E., Vice President">
                                <i class="fa fa-plus"></i>
                            </a>
                            <div id="manager-2-popup-button" class="management-popup">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 pad-l-0 pad-r-0">
                                            <div class="popup-copy-container">
                                                <h2>
                                                    Jason E. Weinstein, <br>
                                                    P.E., Vice President
                                                </h2>
                                                <p>
                                                    Jason is heavily involved with daily schedules, project management, estimating, and design. His engineering licenses include California, Utah, Nevada, Arizona, Oregon and Pennsylvania. He graduated in 1985 from USC with a B.S. in Civil Engineering and Building Science then firmly planted his feet with the company in 1987. When he's not deep in the dirt, you'll find him on the green practicing his short game. At the end of the day, Jason comes home to a loving wife and two beautiful daughters.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 pad-l-0 pad-r-0">
                                            <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'manager-1-popup.jpg'; ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="manager3 wow fadeIn" data-wow-delay="1900ms">
                            <a href="#manager-3-popup-button" class="management-button fancybox" data-toggle="tooltip" data-placement="top" title="George A. Woodley, Founder and President">
                                <i class="fa fa-plus"></i>
                            </a>
                            <div id="manager-3-popup-button" class="management-popup">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 pad-l-0 pad-r-0">
                                            <div class="popup-copy-container">
                                                <h2>
                                                    George A. Woodley, <br>
                                                    Founder and President
                                                </h2>
                                                <p>
                                                    One man's vision has elevated Shoring Engineers to be victorious year after year - George A. Woodley. As the founder and president of the company, his forward-thinking leadership and firm guidance has been the backbone to keeping the business in the black. He champions each project with the most cost-effective and timesaving solutions to ensure success. Five decades later, Shoring is stronger than ever and still poised for continuous growth.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 pad-l-0 pad-r-0">
                                            <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'george-sr.jpg'; ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="manager4 wow fadeIn" data-wow-delay="2100ms">
                            <a href="#manager-4-popup-button" class="management-button fancybox" data-toggle="tooltip" data-placement="top" title="George A. Woodley Jr., P.E., Vice President">
                                <i class="fa fa-plus"></i>
                            </a>
                            <div id="manager-4-popup-button" class="management-popup">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 pad-l-0 pad-r-0">
                                            <div class="popup-copy-container">
                                                <h2>
                                                    George A. Woodley Jr., <br>
                                                    P.E., Vice President
                                                </h2>
                                                <p>
                                                    George Jr. graduated from the University of Southern California with a B.S. in Civil Engineering. He joined Shoring Engineers soon after in 1989, and has lent his expertise to high profile projects such as The Staples Center and the LAC-USC County Hospital, among others. He is also registered in California as a professional engineer. George is an avid fisherman and enjoys tennis.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 pad-l-0 pad-r-0">
                                            <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'george-jr.jpg'; ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="page-banner-title">
                    <aside class="col-sm-3 pad-r-0 pad-l-0 ">
                    </aside>
                    <div class="col-sm-3 pad-r-0 dude-dude duder wow fadeIn unwow pad-l-0 " data-unwow-animation="fadeOut" data-unwow-delay="900ms" data-wow-delay="400ms">
                        ABOUT / management
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <aside class="col-sm-3 pad-r-0 pad-l-0 secondary-left-copy-green wow unwow fadeInUp side-menu" data-unwow-animation="fadeOutDown" data-unwow-delay="1100ms" data-wow-delay="200ms">
                <div>
                    <ul>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/history'; ?>">history&nbsp;
                                <span class="i fa fa-angle-double-right"></span></a></li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/mission'; ?>">mission &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="active historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/management'; ?>">management &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/community-involvement'; ?>">community involvement &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/jason-weinstein'; ?>">jason weinstein &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/testimonials'; ?>">testimonials &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/careers'; ?>">careers &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>

                    </ul>
                </div>
            </aside>

            <div class="col-sm-3 pad-r-0 dude-dude pad-l-0 wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="900ms" data-wow-delay="400ms">
                <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'shoring.jpg'; ?>" alt="">
            </div>
            <div class="col-sm-6  pad-r-0 pad-l-0 secondary-right-copy wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="700ms" data-wow-delay="600ms">
                <article>
                    <h2>
                        From the office to the construction site, our management team provides the vision and leadership to ensure your project's success. </h2>
                    <img class="mar-t-2 mar-b-2" src="<?php echo 'https://shoringengineers.com/assets/images/'.'logo-for-2dary-pages-in-text-area.png'; ?>" alt="">
                    <aside>
                        We work together. We work smarter. And we create detailed plans designed for you and you alone.
                    </aside>
                    <p>
                        With second and third generations of employees on staff that span over five decades, Shoring's success is achieved through careful management of the company.
                    </p>

                </article>
            </div>
        </div>
    </div>
</section>
