<section class="historical-container">
    <div class="historical-timing" data-historical-delay="2100"></div>
    <div class="container-fluid" style="margin-bottom:2px;">
        <div class="row">
            <div class="page-banner-page col-xs-12 pad-r-0 pad-l-0 wow fadeIn unwow " style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'testimonials.jpg'; ?>')" data-unwow-animation="fadeOut" data-unwow-delay="1300ms">

                <div class="page-banner-title">
                    <aside class="col-sm-3 pad-r-0 pad-l-0 ">
                    </aside>
                    <div class="col-sm-3 pad-r-0 dude-dude duder wow fadeIn unwow pad-l-0 " data-unwow-animation="fadeOut" data-unwow-delay="900ms" data-wow-delay="400ms">
                        ABOUT / jason weinstein
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <aside class="col-sm-3 pad-r-0 pad-l-0 secondary-left-copy-green wow unwow fadeInUp side-menu " data-unwow-animation="fadeOutDown" data-unwow-delay="1100ms" data-wow-delay="200ms">
                <div>
                    <ul>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/history'; ?>">history&nbsp;
                                <span class="i fa fa-angle-double-right"></span></a></li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/mission'; ?>">mission &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/management'; ?>">management &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/community-involvement'; ?>">community involvement &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="active historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/jason-weinstein'; ?>">jason weinstein &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/testimonials'; ?>">testimonials &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/careers'; ?>">careers &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>

                    </ul>

                </div>
            </aside>

            <div class="col-sm-3 pad-r-0 dude-dude pad-l-0 wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="900ms" data-wow-delay="400ms">
                <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'jason-weinstein-2.jpg'; ?>" alt="">
            </div>
            <div class="col-sm-6  pad-r-0 pad-l-0 secondary-right-copy wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="700ms" data-wow-delay="600ms">
                <article class="testimonials">
                    <h2>
                        The passing of Jason Weinstein </h2>
                    <img class="mar-t-2 mar-b-2" src="<?php echo 'https://shoringengineers.com/assets/images/'.'logo-for-2dary-pages-in-text-area.png'; ?>" alt="">
                    <p>
                    <blockquote>
                        <p>It is with great sadness that I inform you of the passing of Jason Weinstein on December 15, 2020.  I know I speak for many when I say it was a great privilege and honor to know and work alongside him, and he will be missed immensely. My personal and professional relationship with Jason goes back more than 30 years, and I would like to pay tribute to his achievements and his contributions to the growth and success of Shoring Engineers and Structural Shotcrete Systems.

                            <br><br>After a brief stint working in the Transportation Department for the City of Los Angeles after graduating college, Jason answered a tiny 1”x 3/8” advertisement in the Los Angeles Times Classified section for a Civil Engineer in El Monte.  He officially joined Shoring Engineers in 1986 and found what would become his professional home for the next 34 years. During his impressive tenure, Jason designed and managed hundreds of shoring projects, including Apple headquarters in Cupertino, Los Angeles County Museum of Art, multiple projects at USC, UCLA, Universal Studios, Disneyland and NBC Studios, as well as countless commercial projects in the Los Angeles Basin.  His unwavering dedication to his clients, expert-level knowledge, and business acumen made him a key part of the Shoring Engineers/Structural Shotcrete Systems family and the local construction industry at-large.

                            <br><br>Jason was a licensed Civil Engineer in California, Oregon, Washington and Arizona and had a passion for computer technology and programming.  He used his formidable skills and knowledge to develop a robust business computer operating system and implement many engineering design programs. His crowning engineering/programming achievement was likely the creation of his EarthShore shoring design program. After more than 20 years of using this program, I still marvel at how well it works and will forever think of Jason every time I run it.

                            <br><br>Although Jason had a passion for engineering and computer technology, the true loves of his life were his wife, Amelia (“Amy”), and his two daughters Danielle and Samantha. Jason fell for Amy while they were in college together at USC (Amy must have fallen for him after he roped her into helping him build a concrete canoe for an engineering class project). They wed soon after graduating in 1985. In the early 1990s Danielle and Samantha were born and Jason was smitten once again. Jason was an intentional and loving husband and father; he traveled extensively with his family, visiting all 13 presidential libraries, all 21 California missions and many states and countries. Jason’s love of sports was shared by his wife and daughters. He watched as Amy coached their daughters’ youth sports teams, and as a family they followed women’s soccer, the NHL’s Anaheim Ducks and college sports. Football occasionally presented some house-dividing but good-natured rivalries; among the schools represented in the family were USC (Jason, Amy and Danielle), UCLA (Danielle), UC Davis (Samantha), and Michigan State (Samantha).  Jason’s office had numerous photos of the Weinstein women and other family members on display, a testament to the priority they held in his life.

                            <br><br>Jason was a lifelong Bruce Springsteen fan (he attended several of his concerts), which fueled his desire to play the guitar.  His passion for music was also shared by his daughter Samantha and his nephew Frank, whom Jason taught to play.  When he wasn’t playing guitar, traveling or enjoying various concerts with his family in his free time, Jason could be found enjoying time on the golf course or playing the occasional hand of pai gow poker.

                            <br><br>Jason also gave back to the civil engineering and construction communities by being very active in ASCE, DFI, and ADSC.  In addition, he provided his expertise, knowledge and resources to the Shotcrete Union. Jason was a regular exhibitor at DFI conventions and attended many trade shows over the years. His presence and contributions there will be missed.

                            <br><br>We will all miss Jason greatly. I hope you will join me in celebrating his life by living your days well, telling your loved ones that you love them, and never taking for granted the many blessings we have in our lives.  Should you wish to make a donation to remember Jason, please donate to St. Jude’s Medical Center in his name.
                        <footer>
                            Sincerely,<br>

                            George A. Woodley, Jr., President<br>
                            Shoring Engineers<br>
                            Structural Shotcrete Systems, Inc.<br>

                        </footer>
                    </blockquote>
                    </p>
                </article>
            </div>
        </div>
    </div>
</section>
