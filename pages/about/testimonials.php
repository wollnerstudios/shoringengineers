<section class="historical-container">
    <div class="historical-timing" data-historical-delay="2100"></div>
    <div class="container-fluid" style="margin-bottom:2px;">
        <div class="row">
            <div class="page-banner-page col-xs-12 pad-r-0 pad-l-0 wow fadeIn unwow " style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'testimonials.jpg'; ?>')" data-unwow-animation="fadeOut" data-unwow-delay="1300ms">

                <div class="page-banner-title">
                    <aside class="col-sm-3 pad-r-0 pad-l-0 ">
                    </aside>
                    <div class="col-sm-3 pad-r-0 dude-dude duder wow fadeIn unwow pad-l-0 " data-unwow-animation="fadeOut" data-unwow-delay="900ms" data-wow-delay="400ms">
                        ABOUT / testimonials
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <aside class="col-sm-3 pad-r-0 pad-l-0 secondary-left-copy-green wow unwow fadeInUp side-menu " data-unwow-animation="fadeOutDown" data-unwow-delay="1100ms" data-wow-delay="200ms">
                <div>
                    <ul>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/history'; ?>">history&nbsp;
                                <span class="i fa fa-angle-double-right"></span></a></li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/mission'; ?>">mission &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/management'; ?>">management &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/community-involvement'; ?>">community involvement &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/jason-weinstein'; ?>">jason weinstein &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="active historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/testimonials'; ?>">testimonials &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/careers'; ?>">careers &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>

                    </ul>

                </div>
            </aside>

            <div class="col-sm-3 pad-r-0 dude-dude pad-l-0 wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="900ms" data-wow-delay="400ms">
                <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'testimonials-2.jpg'; ?>" alt="">
            </div>
            <div class="col-sm-6  pad-r-0 pad-l-0 secondary-right-copy wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="700ms" data-wow-delay="600ms">
                <article class="testimonials">
                    <h2>
                        Look what people are saying about Shoring Engineers. </h2>
                    <img class="mar-t-2 mar-b-2" src="<?php echo 'https://shoringengineers.com/assets/images/'.'logo-for-2dary-pages-in-text-area.png'; ?>" alt="">
                    <p>
                    <blockquote>
                        <p>"Shoring Engineers has set the tone of hard work, creativity, and integrity to survive in this challenging industry. Up and down the coast from the California State Office Building in San Francisco to Los Angeles City Hall to Ventura County Medical Center, we have seen the breadth of your company and the commitment you bring to every project. In a region where we believe subcontractors are some of the best in the country, we consider you to be family. Congratulations on your notable milestone, and we look forward to the next 50 years."</p>
                        <footer>
                            James M. McLamb, Sr. Vice President/Regional Executive Manager<br/>
                            The Clark Construction Group, California LP
                        </footer>
                    </blockquote>
                    <blockquote>
                        <p>"We would like to commend Shoring Engineers for the exceptional services they have provided during the past 30 years. Every level of your organization has exceeded our expectations and provided our clients with strength, consistency, and unprecedented value. Even with the most complicated project site logistics you have executed flawlessly, provided exceptional planning, putting all stake holders at ease."</p>
                        <footer>
                            Rashmi Mehta, Vice President <br/>
                            Hathaway Dinwiddie Construction Company
                        </footer>
                    </blockquote>
                    <blockquote>
                        <p>"We have worked with Shoring Engineers on several projects over the past 15 years and consider them one of the best shoring contractors in Southern California. As a general contractor, our success depends on the quality of our subcontractors. Shoring has helped our company be successful through their competitive pricing, quality workmanship, and creative problem solving when challenges arise. Both their office and field personnel are honest, helpful, and equitable - from the estimating phase through project closeout. We look forward to working on many more projects together."</p>
                        <footer>
                            Ryan Hupf, Senior Project Manager<br/>
                            Morley Construction Company
                        </footer>
                    </blockquote>
                    <blockquote>
                        <p>"We have had a long-term relationship with Shoring Engineers that dates back to 1988. As a design build commercial firm, we have come to depend on their expertise in dealing with some very challenging sites throughout Southern California and have worked directly with Jason on both shoring and mass excavation portions of these projects. Shoring Engineers is very price competitive and we would highly recommend them."</p>
                        <footer>
                            John Brown, Chief Estimator<br/>
                            McCleskey Construction
                        </footer>
                    </blockquote>
                    </p>
                </article>
            </div>
        </div>
    </div>
</section>
