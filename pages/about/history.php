<section class="historical-container">
    <div class="historical-timing" data-historical-delay="2100"></div>
    <div class="container-fluid" style="margin-bottom:2px;">
        <div class="row">
            <div class="page-banner-page col-xs-12 pad-r-0 pad-l-0 wow fadeIn unwow " style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'history.jpg'; ?>')" data-unwow-animation="fadeOut" data-unwow-delay="1300ms">

                <div class="page-banner-title">
                    <aside class="col-sm-3 pad-r-0 pad-l-0 ">
                    </aside>
                    <div class="col-sm-3 pad-r-0 dude-dude duder wow fadeIn unwow pad-l-0 " data-unwow-animation="fadeOut" data-unwow-delay="900ms" data-wow-delay="400ms">
                        ABOUT / history
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <aside class="col-sm-3 pad-r-0 pad-l-0 secondary-left-copy-green wow unwow fadeInUp side-menu " data-unwow-animation="fadeOutDown" data-unwow-delay="1100ms" data-wow-delay="200ms">
                <div>
                    <ul>
                        <li>
                            <a class="active historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/history'; ?>">history&nbsp;
                                <span class="i fa fa-angle-double-right"></span></a></li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/mission'; ?>">mission &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/management'; ?>">management &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/community-involvement'; ?>">community involvement &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/jason-weinstein'; ?>">jason weinstein &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/testimonials'; ?>">testimonials &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'about/careers'; ?>">careers &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>

                    </ul>

                </div>
            </aside>

            <div class="col-sm-3 pad-r-0 dude-dude pad-l-0 wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="900ms" data-wow-delay="400ms">
                <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'george-woodley.jpg'; ?>" alt="">
            </div>
            <div class="col-sm-6  pad-r-0 pad-l-0 secondary-right-copy wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="700ms" data-wow-delay="600ms">
                <article>
                    <h2>
                        A rich history of digging up dirt. </h2>
                    <img class="mar-t-2 mar-b-2" src="<?php echo 'https://shoringengineers.com/assets/images/'.'logo-for-2dary-pages-in-text-area.png'; ?>" alt="">
                    <aside>
                        We're five decades deep into getting our hands dirty. Our founder, George A. Woodley, started Shoring Engineers back in 1966 and hasn't stopped construction ever since.
                    </aside>
                    <p>
                        We're five decades deep into getting our hands dirty. Our founder, George A. Woodley, started Shoring Engineers back in 1966 and hasn't stopped construction ever since. With a portfolio of over 1,000 projects, our accumulated knowledge in drilling, shoring, excavating, and shotcrete is bar none. We've boldly looked ahead and are responsible for many of the innovations and tooling adaptations that have not only revolutionized the industry, but increased efficiencies as well. Which translated to substantial savings for several of our clients.
                        <br><br>
                        Inside, excellent teamwork is the key to our success. The tenure of the top brass is unparalleled. We get down and dirty, work hard and smart, then build upon our expertise by participating in professional organizations such as the Deep Foundations Institute (DFI), the International Association of Foundation Drilling (ADSC), and American Society of Civil Engineers. Our roots are firmly planted as we continue to dig into every new challenge that comes knocking on our door.
                    </p>

                </article>
            </div>
        </div>
    </div>
</section>
