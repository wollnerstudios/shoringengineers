<?php $mail = isset($_GET['mail']) ? $_GET['mail'] : null; ?>
<!--<?php print_r($mail); ?> -->
<section class="historical-container">
    <div class="historical-timing" data-historical-delay="2100"></div>
    <div class="container-fluid" style="margin-bottom:2px;">
        <div class="row">
            <div class="page-banner-page col-xs-12 pad-r-0 pad-l-0 wow fadeIn unwow" style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'contact-top.jpg'; ?>')" data-unwow-animation="fadeOut" data-unwow-delay="1300ms">

                <div class="page-banner-title">
                    <aside class="col-sm-3 pad-r-0 pad-l-0 ">
                    </aside>
                    <div class="col-sm-3 pad-r-0 dude-dude duder wow fadeIn unwow pad-l-0 " data-unwow-animation="fadeOut" data-unwow-delay="900ms" data-wow-delay="400ms">
                        CONTACT
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <aside class="col-sm-3 pad-r-0 pad-l-0 secondary-left-copy-green wow unwow fadeInUp " data-unwow-animation="fadeOutDown" data-unwow-delay="1100ms" data-wow-delay="200ms">
                <div>
                    <h3>We are licenced in 6 states:</h3>
                    <ul>
                        <li>CA Lic. #245416-A</li>
                        <li>AZ Lic. #ROC124501</li>
                        <li>NV Lic. #0055754</li>
                        <li>OR Lic. #188382</li>
                        <li>UT Lic. #312403-5501</li>
                        <li>WA Lic. #2610</li>


                    </ul>


                </div>
            </aside>

            <div class="col-sm-3 pad-r-0 dude-dude pad-l-0 wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="900ms" data-wow-delay="400ms">
                <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'shoring-brochure.jpg'; ?>" alt="">
            </div>
            <div class="col-sm-6  pad-r-0 pad-l-0 secondary-right-copy wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="700ms" data-wow-delay="600ms">
                <article>
                    <h2>
                        Increased safety, increased peace of mind. </h2>
                    <img class="mar-t-2 mar-b-2" src="<?php echo 'https://shoringengineers.com/assets/images/'.'logo-for-2dary-pages-in-text-area.png'; ?>" alt="">
                    <aside>
                        Call 562-944-9331, fax 562-941-9331, or drop by 12645 Clark Street in Santa Fe Springs.
                    </aside>

                    <p>
                        Need more information on Shoring, Caissons, Excavation, or Shotcrete? Want us to bid on your next project? Simply call or fill out the contact form and someone will get in touch with you shortly.

                    </p>
                    <?php if ($mail == "success"): ?>
                        <div class="alert alert-success">
                            Message has been sent! Thank you.
                        </div>
                    <?php endif; ?>
                    <?php if ($mail == "fail"): ?>
                        <div class="alert alert-warning">
                            Oops! Looks like something went wrong!
                        </div>
                    <?php endif; ?>
                    <form method="post" enctype="multipart/form-data" action="<?php echo 'https://shoringengineers.com/'.'mail/contact.php'; ?>" class="contact-form">


                        <div class="row">
                            <div class="col-xs-12">
                                <input type="text" class="form-control floating-label" placeholder="Name *" name="name" required>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="text" class="form-control floating-label" placeholder="Company" name="company">

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="tel" class="form-control floating-label" placeholder="Phone" name="phone">

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="email" class="form-control floating-label" placeholder="Email *" name="email" required>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="text" class="form-control floating-label" placeholder="Address" name="address">

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="text" class="form-control floating-label" placeholder="City" name="city">

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="text" class="form-control floating-label" placeholder="Zip" name="zip">

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="contact-label">
                                    <label for="details">Project Details</label><br>

                                </div>
                                <textarea class="contact-input" placeholder="Details" name="details"></textarea>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="contact-label">
                                    <label for="request_date"><i class="fa fa-calendar"></i> Date Requested</label>

                                </div>
                                <input type="text" class="date-picker contact-input" name="request_date">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="contact-label">
                                    <label for="files">Upload file</label>
                                </div>
                                <div class="btn btn-primary file-upload">
                                    <span>SELECT</span>
                                    <input type="file" accept=".pdf, .doc, .docx, .gif, .jpg, .jpeg, .png, .txt, .csv" name="files" id="contact_files">
                                </div>


                            </div>


                        </div>

                        <input class="col-xs-12" type="submit" value="SUBMIT">

                    </form>

                </article>
            </div>
        </div>
    </div>
</section>
