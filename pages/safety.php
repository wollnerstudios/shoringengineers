<section class="historical-container">
    <div class="historical-timing" data-historical-delay="2100"></div>
    <div class="container-fluid" style="margin-bottom:2px;">
        <div class="row">
            <div class="page-banner-page col-xs-12 pad-r-0 pad-l-0 wow fadeIn unwow" style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'safety.jpg'; ?>')" data-unwow-animation="fadeOut" data-unwow-delay="1300ms">

                <div class="page-banner-title">
                    <aside class="col-sm-3 pad-r-0 pad-l-0 ">
                    </aside>
                    <div class="col-sm-3 pad-r-0 dude-dude duder wow fadeIn unwow pad-l-0 " data-unwow-animation="fadeOut" data-unwow-delay="900ms" data-wow-delay="400ms">
                        SAFETY
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <aside class="col-sm-3 pad-r-0 pad-l-0 secondary-left-copy-green wow unwow fadeInUp" data-wow-delay="200ms" data-unwow-animation="fadeOutDown" data-unwow-delay="1100ms">
                <div>
                    <h3>Our safety training includes:</h3>
                    <ul>
                        <li>Fall protection</li>
                        <li>CPR/first aid</li>
                        <li>Scaffolding awareness</li>
                        <li>Hazardous materials</li>
                        <li>Forklift operation</li>
                        <li>Excavation competent person training</li>
                        <li>Trench shoring</li>
                        <li>Rigging</li>
                        <li>Traffic control</li>
                        <li>Hazwopper (Hazardous Waste Operations and Emergency Response)</li>
                    </ul>
                </div>
            </aside>

            <div class="col-sm-3 pad-r-0 dude-dude pad-l-0 wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="900ms" data-wow-delay="400ms">
                <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'kevin.jpg'; ?>" alt="">
            </div>
            <div class="col-sm-6  pad-r-0 pad-l-0 secondary-right-copy wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="700ms" data-wow-delay="600ms">
                <article>
                    <h2>
                        Increased safety, increased peace of mind. </h2>
                    <img class="mar-t-2 mar-b-2" src="<?php echo 'https://shoringengineers.com/assets/images/'.'logo-for-2dary-pages-in-text-area.png'; ?>" alt="">
                    <aside>
                        From fall protection to CPR/first aid, scaffolding awareness to hazardous materials-our impressive track record of onsite safety didn't happen by accident.
                    </aside>

                    <p>
                        We have a comprehensive safety program firmly in place and are constantly working to make it even better. At the helm, you find Kevin Winger our safety manager who comes from the oil industry where safety standards and procedures are the most stringent on earth. He's constantly moving all over our construction sites taking precautionary and corrective measures to maximize safety at each job. When he's not onsite, Kevin is leading the training classes educating our staff.
                        <br><br>
                        Since most of our projects start at ground zero, we implement a zero tolerance policy for lost time accidents and near miss incidents. It starts at the top levels of management down to the workers in the field. Every manager and every employee is on high alert to ensure that not only he or she is working safely, but the others around them are as well. By using the proper equipment for the job while watching each other's backs, we're creating a safe and accident-free working environment. Respect for teamwork, camaraderie amongst co-workers, and a set of eyes always on the lookout-you can rest assured our increased safety measures will give you an increased peace of mind.
                    </p>

                </article>
            </div>
        </div>
    </div>
</section>
