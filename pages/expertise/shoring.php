<section class="historical-container">
    <div class="historical-timing" data-historical-delay="2100"></div>
    <div class="container-fluid" style="margin-bottom:2px;">
        <div class="row">
            <div class="page-banner-page col-xs-12 pad-r-0 pad-l-0 wow fadeIn unwow" style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'shoring-1.jpg'; ?>')" data-unwow-animation="fadeOut" data-unwow-delay="1300ms">

                <div class="page-banner-title">
                    <aside class="col-sm-3 pad-r-0 pad-l-0 ">
                    </aside>
                    <div class="col-sm-3 pad-r-0 dude-dude duder wow fadeIn unwow pad-l-0 " data-unwow-animation="fadeOut" data-unwow-delay="900ms" data-wow-delay="400ms">
                        EXPERTISE / shoring
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <aside class="col-sm-3 pad-r-0 pad-l-0 secondary-left-copy-green wow unwow fadeInUp side-menu " data-unwow-animation="fadeOutDown" data-unwow-delay="1100ms" data-wow-delay="200ms">
                <div>
                    <ul>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/project-management'; ?>">project management &nbsp;
                                <span class="i fa fa-angle-double-right"></span></a></li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/markets'; ?>">markets &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="active historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/shoring'; ?>">shoring &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/excavation'; ?>">excavation &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/caissons'; ?>">caissons &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/shotcrete'; ?>">shotcrete &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>

                    </ul>

                </div>
            </aside>

            <div class="col-sm-3 pad-r-0 dude-dude pad-l-0 wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="900ms" data-wow-delay="400ms">
                <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'shoring-2.jpg'; ?>" alt="">
            </div>
            <div class="col-sm-6  pad-r-0 pad-l-0 secondary-right-copy wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="700ms" data-wow-delay="600ms">
                <article>
                    <h2>
                        It's a shore thing. </h2>
                    <img class="mar-t-2 mar-b-2" src="<?php echo 'https://shoringengineers.com/assets/images/'.'logo-for-2dary-pages-in-text-area.png'; ?>" alt="">
                    <aside>
                        From CAD drawings to temporary soldier beams, tieback anchors to tangent walls - we know a thing or two about shoring.
                    </aside>
                    <p>
                        It's how we started and certainly one of our core strengths. Our approach? We work smarter - anticipating the unknown - then collaborate to resolve the issues through innovative thinking and execution. Whether we're only going 5-foot down or 85-feet deep, even if your foundation is in a tight spot - no problem. We once successfully completed a 50-foot deep shoring and excavation project in a tightly congested section of Santa Monica. All because we had the knowledge of existing underground beams we had installed nearly three decades earlier. How's that for real-world expertise?
                    </p>

                </article>
            </div>
        </div>
    </div>
</section>
