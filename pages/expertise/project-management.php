<section class="historical-container">
    <div class="historical-timing" data-historical-delay="2100"></div>
    <div class="container-fluid" style="margin-bottom:2px;">
        <div class="row">
            <div class="page-banner-page col-xs-12 pad-r-0 pad-l-0 wow fadeIn unwow" style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'project-management.jpg'; ?>')" data-unwow-animation="fadeOut" data-unwow-delay="1300ms">

                <div class="page-banner-title">
                    <aside class="col-sm-3 pad-r-0 pad-l-0 ">
                    </aside>
                    <div class="col-sm-3 pad-r-0 dude-dude duder wow fadeIn unwow pad-l-0 " data-unwow-animation="fadeOut" data-unwow-delay="900ms" data-wow-delay="400ms">
                        EXPERTISE / project management
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <aside class="col-sm-3 pad-r-0 pad-l-0 secondary-left-copy-green wow unwow fadeInUp side-menu " data-unwow-animation="fadeOutDown" data-unwow-delay="1100ms" data-wow-delay="200ms">
                <div>
                    <ul>
                        <li>
                            <a class="active historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/project-management'; ?>">project management &nbsp;
                                <span class="i fa fa-angle-double-right"></span></a></li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/markets'; ?>">markets &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/shoring'; ?>">shoring &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/excavation'; ?>">excavation &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/caissons'; ?>">caissons &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/shotcrete'; ?>">shotcrete &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>

                    </ul>

                </div>
            </aside>

            <div class="col-sm-3 pad-r-0 dude-dude pad-l-0 wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="900ms" data-wow-delay="400ms">
                <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'project-management-2.jpg'; ?>" alt="">
            </div>
            <div class="col-sm-6  pad-r-0 pad-l-0 secondary-right-copy wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="700ms" data-wow-delay="600ms">
                <article>
                    <h2>
                        Taking care to plan the project in every detail. </h2>
                    <img class="mar-t-2 mar-b-2" src="<?php echo 'https://shoringengineers.com/assets/images/'.'logo-for-2dary-pages-in-text-area.png'; ?>" alt="">
                    <aside>
                        Each project's success is achieved through careful management.
                    </aside>
                    <p>
                        By working together we work smarter and produce truly amazing results the first time out. Some of our most breakthrough solutions happen by making the leap from need to innovation. Other impressive feats come about by ignoring the way it's always been done to figuring out how it can be done better. Whether indoors behind a desk or outdoors next to a drill rig, our engineers have the instinct and knowledge to develop on-the-spot adaptations when any unforeseen issues arise. This thought leadership approach to our project management has resulted in many industry-wide uses of our innovative designs.
                    </p>

                </article>
            </div>
        </div>
    </div>
</section>
