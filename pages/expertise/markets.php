<section class="historical-container">
    <div class="historical-timing" data-historical-delay="2100"></div>
    <div class="container-fluid" style="margin-bottom:2px;">
        <div class="row">
            <div class="page-banner-page col-xs-12 pad-r-0 pad-l-0 wow fadeIn unwow" style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'los-angeles.jpg'; ?>')" data-unwow-animation="fadeOut" data-unwow-delay="1300ms">

                <div class="page-banner-title">
                    <aside class="col-sm-3 pad-r-0 pad-l-0 ">
                    </aside>
                    <div class="col-sm-3 pad-r-0 dude-dude duder wow fadeIn unwow pad-l-0 " data-unwow-animation="fadeOut" data-unwow-delay="900ms" data-wow-delay="400ms">
                        EXPERTISE / markets
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <aside class="col-sm-3 pad-r-0 pad-l-0 secondary-left-copy-green wow unwow fadeInUp side-menu " data-unwow-animation="fadeOutDown" data-unwow-delay="1100ms" data-wow-delay="200ms">
                <div>
                    <ul>
                        <li>
                            <a class=" historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/project-management'; ?>">project management &nbsp;
                                <span class="i fa fa-angle-double-right"></span></a></li>
                        <li>
                            <a class="active historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/markets'; ?>">markets &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/shoring'; ?>">shoring &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/excavation'; ?>">excavation &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/caissons'; ?>">caissons &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>
                        <li>
                            <a class="historical" data-historical-delay="2100" href="<?php echo 'https://shoringengineers.com/'.'expertise/shotcrete'; ?>">shotcrete &nbsp;<span class="i fa fa-angle-double-right"></span></a>
                        </li>

                    </ul>

                </div>
            </aside>

            <div class="col-sm-3 pad-r-0 dude-dude pad-l-0 wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="900ms" data-wow-delay="400ms">
                <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'shoring-truck.jpg'; ?>" alt="">
            </div>
            <div class="col-sm-6  pad-r-0 pad-l-0 secondary-right-copy wow unwow fadeInUp" data-unwow-animation="fadeOutDown" data-unwow-delay="700ms" data-wow-delay="600ms">
                <article>
                    <h2>
                        Building on fifty years of experience. </h2>
                    <img class="mar-t-2 mar-b-2" src="<?php echo 'https://shoringengineers.com/assets/images/'.'logo-for-2dary-pages-in-text-area.png'; ?>" alt="">
                    <aside>
                        Just outside of Los Angeles, Shoring proudly serves California, Arizona, Nevada, Oregon, Utah, and Washington.
                    </aside>
                    <p>
                        With over five decades of doing shoring, caisson, excavating and shotcrete, there's no question that we have experts in every aspect of foundation construction. Our firm has unmatched knowledge implementing innovative and cost-effective design build solutions for projects. Whether private to commercial, educational institutions to government projects, we've worked in all sectors successfully serving customers throughout Southern California and the surrounding West Coast states.
                    </p>

                </article>
            </div>
        </div>
    </div>
</section>
