<div class="historical-container">
    <div class="historical-timing" data-historical-delay="1700"></div>
    <section class="gallery-banner hidden-xs">
        <div class="container-fluid" style="margin-bottom:2px;">
            <div class="row">
                <div class="col-xs-12 pad-r-0 pad-l-0 page-banner-page wow unwow fadeIn" style="background-image:url('<?php echo 'https://shoringengineers.com/assets/images/'.'gallery-top-banner.jpg'; ?>')" data-unwow-animation="fadeOut" data-unwow-delay="1200ms">

                    <div class="gallery-banner-text">
                        <h1>
                            We have no problem getting our hands dirty.
                        </h1>
                        <span> Current and past projects </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    $galleryImages = [];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/5fwy-norwalk.jpg',
        'originalURL' => 'assets/images/gallery/5fwy-norwalk.jpg',
        'alt' => 'alt',
        'title' => 'Route 5',
        'subtitle' => 'Santa Fe Springs'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/USC_Michelson_Center_For_Convergent_Bioscience-20150818-105029.jpg',
        'originalURL' => 'assets/images/gallery/USC_Michelson_Center_For_Convergent_Bioscience-20150818-105029.jpg',
        'alt' => 'alt',
        'title' => 'USC Michelson Center',
        'subtitle' => 'Los Angeles'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/IMG_1699.jpg',
        'originalURL' => 'assets/images/gallery/IMG_1699.jpg',
        'alt' => 'alt',
        'title' => 'USC Michelson Center.',
        'subtitle' => 'Los Angeles'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/IMG_2357.jpg',
        'originalURL' => 'assets/images/gallery/IMG_2357.jpg',
        'alt' => 'alt',
        'title' => 'USC Michelson Center',
        'subtitle' => 'Los Angeles'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/IMG_1901.jpg',
        'originalURL' => 'assets/images/gallery/IMG_1901.jpg',
        'alt' => 'alt',
        'title' => 'Permanent Soldier Beam Retaining Wall',
        'subtitle' => '1239 Victoria Street, Costa Mesa'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/IMG_1903.jpg',
        'originalURL' => 'assets/images/gallery/IMG_1903.jpg',
        'alt' => 'alt',
        'title' => 'Permanent Soldier Beam Retaining Wall',
        'subtitle' => '1239 Victoria Street, Costa Mesa'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/WeberMetals-LongBeach-1.jpg',
        'originalURL' => 'assets/images/gallery/WeberMetals-LongBeach-1.jpg',
        'alt' => 'alt',
        'title' => ' Weber Metals',
        'subtitle' => ' Long Beach'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/WeberMetals-LongBeach-2.jpg',
        'originalURL' => 'assets/images/gallery/WeberMetals-LongBeach-2.jpg',
        'alt' => 'alt',
        'title' => 'Weber Metals',
        'subtitle' => 'Long Beach'
    ];

    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/IMG_0057.jpg',
        'originalURL' => 'assets/images/gallery/IMG_0057.jpg',
        'alt' => 'alt',
        'title' => 'Route 15/215',
        'subtitle' => 'Devore',
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/IMG_3725.jpg',
        'originalURL' => 'assets/images/gallery/IMG_3725.jpg',
        'alt' => 'alt',
        'title' => 'Route 15/215',
        'subtitle' => 'Devore'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/IMG_2179t.jpg',
        'originalURL' => 'assets/images/gallery/IMG_2179t.jpg',
        'alt' => 'alt',
        'title' => 'Wilshire and Hobart',
        'subtitle' => 'Los Angeles'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/Wilshire-mid-city.jpg',
        'originalURL' => 'assets/images/gallery/Wilshire-mid-city.jpg',
        'alt' => 'alt',
        'title' => 'Wilshire and Hobart',
        'subtitle' => 'Los Angeles'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/spot-12-buckley.jpg',
        'originalURL' => 'assets/images/gallery/spot-12-buckley.jpg',
        'alt' => 'alt',
        'title' => 'Shotcrete Walls at Buckley School',
        'subtitle' => '3900 Stansbury Avenue, Sherman Oaks'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/spot-13-buckley.jpg',
        'originalURL' => 'assets/images/gallery/spot-13-buckley.jpg',
        'alt' => 'alt',
        'title' => 'Shotcrete Walls at Buckley School',
        'subtitle' => '3900 Stansbury Avenue, Sherman Oaks'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/IMG_0246.jpg',
        'originalURL' => 'assets/images/gallery/IMG_0246.jpg',
        'alt' => 'alt',
        'title' => 'Peterson Automotive Museum',
        'subtitle' => 'Los Angeles'
    ];
    $galleryImages[] = [
        'thumbURL' => 'assets/images/gallery/IMG_1836.jpg',
        'originalURL' => 'assets/images/gallery/IMG_1836.jpg',
        'alt' => 'alt',
        'title' => 'Peterson Automotive Museum',
        'subtitle' => 'Los Angeles'
    ];
    ?>
    <div class="gallery-concontainer">

        <section class="shoring-gallery-mobile visible-xs">
            <div class="slick-slider-gallery">
                <?php foreach ($galleryImages as $galleryImage): ?>
                    <div class="gallery-slide">
                        <div class="gallery-slide-image" style="background-image:url('<?php echo $galleryImage['originalURL']; ?>')"></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </section>
        <section class="visible-xs">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="gallery-mobile-copy">
                            <h2>We have no problem getting our hands dirty.</h2>
                            <span>Current and past projects</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="shoring-gallery hidden-xs">
            <?php $i = 1125; ?>
            <?php $j = 1; ?>
            <?php $k = 100; ?>
            <?php foreach ($galleryImages as $galleryImage): ?>
                <a href="<?php echo $galleryImage['originalURL']; ?>"
                   class="image-popup-no-margins mix fancybox-gallery unwow wow fadeIn"
                   data-unwow-animation="fadeOut"
                   data-wow-delay="<?php echo $k; ?>ms"
                   data-unwow-delay="<?php echo $i; ?>ms"
                   rel="fancybox-thumb"
                   style="background-image:url('<?php echo $galleryImage['originalURL']; ?>')">

                    <div id="gallery-thumbnail-<?php echo $j; ?>" class="gallery-thumbnail-popup">
                        <img src="<?php echo $galleryImage['originalURL']; ?>" alt="">
                    </div>
                    <div class="black-overlay-baby">
                        <div class="black-overlay-copy-wrapper">
                            <div class="title-black-overlay">
                                <?php echo $galleryImage['title']; ?>
                            </div>
                            <div class="hoverview">
                                <img src="<?php echo 'https://shoringengineers.com/assets/images/'.'lupe2.png'; ?>" alt="">
                            </div>
                            <div class="subtitle-black-overlay">
                                <?php echo $galleryImage['subtitle']; ?>
                            </div>
                        </div>
                    </div>
                </a>
                <?php $i -= 75; ?>
                <?php $j++; ?>
                <?php $k += 50; ?>
            <?php endforeach; ?>
        </section>
    </div>
</div>
