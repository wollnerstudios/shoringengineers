
L.mapbox.accessToken = 'pk.eyJ1Ijoid29sbG5lcnN0dWRpb3MiLCJhIjoiY2lmOTE5bm11MGdobHN1a252eHQyN3RhYSJ9.ReTWRQd_NEgYvYdKLkiXoA';
var map = L.mapbox.map('map', null, {zoomControl: false, maxZoom: 18})
    .setView([34, -118], 10);

var layer_styles = {
    // Streets: L.mapbox.tileLayer('mapbox.streets'),
    // Light: L.mapbox.tileLayer('mapbox.light'),
    Satellite: L.mapbox.tileLayer('mapbox.streets-satellite'),
    // Pencil: L.mapbox.tileLayer('mapbox.pencil'),
    Shoring: L.mapbox.tileLayer('wollnerstudios.02f6f0d2'),
};

var layers = document.getElementById('menu-ui');

// addLayer(L.mapbox.tileLayer('wollnerstudios.02f6f0d2'), 'Base Map', 2);

addLayer(L.mapbox.tileLayer('mapbox.streets-satellite'), 'Satellite', 1);

function addLayer(layer, name, zIndex) {
  // layer
  //   .setZIndex(zIndex)
  //   .addTo(map);

    var link = document.createElement('a');
        link.href = '#';
        // link.className = 'active';
        link.innerHTML = name;
    link.onclick = function(e) {
      e.preventDefault();
      e.stopPropagation();

      if(map.hasLayer(layer)) {
        map.removeLayer(layer);
        map.addLayer(layer_styles['Shoring']);
        this.className = '';
      } else {
        map.removeLayer(layer_styles['Shoring']);
        map.addLayer(layer);
        this.className = 'active';
      }
    };
  layers.appendChild(link);
}

// var overlays = {
//   "Roads": roadsLayer
// }
// layers.Shoring.addTo(map);
//layers.Streets.addTo(map);
// layers.Streets.setZIndex(2).addTo(map);
// L.control.layers(layers).addTo(map);


new L.Control.Zoom({ position: 'bottomleft' }).addTo(map);    

// load map points
// var myLayer = omnivore.csv("/custom/joblist_full.csv", null, L.mapbox.featureLayer().addTo(map).on('error', function(error){
//   console.log(error);
// }));
console.log(map);


var myLayer = omnivore.csv("/custom/joblist_full.csv", null, L.mapbox.featureLayer());


var info = document.getElementById('info');



// Iterate through each feature layer item, build a
// marker menu item and enable a click event that pans to + opens
// a marker that's associated to the marker item.

myLayer.on('ready', function (e) {
  // var searchString = $('#search').val().toLowerCase();

  var clusterGroup = new L.MarkerClusterGroup({
    polygonOptions: { weight: 2, color: '#006f51', opacity: 0.2 }
  });
  // console.log(geojson);
  e.target.eachLayer(function (layer) {
    var str = layer.feature.properties.title;
    // console.log(str.toLowerCase());
    // console.log($('#search').val());
    // console.log(str.toLowerCase().indexOf(searchString));
    // if(true){
      layer.setIcon(L.mapbox.marker.icon({
        'marker-color' : '#006f51'
      }));
      clusterGroup.addLayer(layer);
      
      var mylink = info.appendChild(document.createElement('a'));
      mylink.className = 'item';
      mylink.href = '#';
      mylink.id = layer.feature.properties.marker_id;
      
      var imagetext = (layer.feature.properties.image)?'<div class="project-list-image"><img src="/images/map/'+layer.feature.properties.image+'"/></div>':'';
      var popupimagetext = (layer.feature.properties.image)?'<div class="project-list-image"><img src="/images/map/'+layer.feature.properties.image+'" /></div>':'';

      var popuptext = 
        layer.feature.properties.title +
        popupimagetext +
        '<small>' + 
        layer.feature.properties.description.toLowerCase() + 
        '<br/>' +
        layer.feature.properties.Customer.toLowerCase() +
        '</small>';
        layer.bindPopup(popuptext, {keepInView: true});

      
      mylink.innerHTML = 
        layer.feature.properties.title +
        imagetext +
        '<small>' + 
        layer.feature.properties.description.toLowerCase() + 
        '<br/>' +
        layer.feature.properties.Customer.toLowerCase() +
        '</small>';
      

      mylink.onclick = function() {
        if (/active/.test(this.className)) {
          this.className = this.className.replace(/active/, '').replace(/\s\s*$/, '');
        } else {
          var siblings = info.getElementsByTagName('a');
          for (var i = 0; i < siblings.length; i++) {
            siblings[i].className = siblings[i].className
              .replace(/active/, '').replace(/\s\s*$/, '');
          };
          this.className += ' active';
   
          map.panTo(layer.getLatLng());
          map.setZoom(18);
          map.panTo(layer.getLatLng());
          layer.openPopup();


        }
        return false;
      };

      layer.on('click', function(e){
        // alert(layer.feature.properties.marker_id);
        if (/active/.test(mylink.className)) {
          mylink.className = mylink.className.replace(/active/, '').replace(/\s\s*$/, '');
        } else {
          var siblings = info.getElementsByTagName('a');
          for (var i = 0; i < siblings.length; i++) {
            siblings[i].className = siblings[i].className
              .replace(/active/, '').replace(/\s\s*$/, '');
          };
          mylink.className += ' active';

          map.setZoom(18);
          map.panTo(layer.getLatLng());
          // layer.openPopup();
        }

        // Scroll selected item to top of list
        $('a.item.active').show();
        var linktop = $('a.item.active').offset().top;
        var scrollpos = $('#info').scrollTop();
        $('#info').animate({
          scrollTop: scrollpos+linktop-40
        }, 500);
        // end scroll

        return false;    

    }); 

    // }

  });// end layer each
  map.addLayer(layer_styles['Shoring']);
  map.addLayer(clusterGroup);

// list of hidden projects (state of list just before each search function call)
var searchState = "";
var stateList = [];
var hiddenList = [];

$('#search').keyup(function(e){

  // stateList = [];
  // hiddenList = [];
  // console.log(stateList);
  $("#info a:hidden").each(function(){
    hiddenList.push(this.id);
  });
  //console.log(hiddenList);

  search();
 
  //console.log(stateList);
  $(hiddenList).not(stateList).get();
    console.log('equal');

  $.each(stateList, function(i,val){
    $(val).hide();
  });


    
});

// show projects previously hidden
//setup before functions

var typingTimer;                //timer identifier
var doneTypingInterval = 1000;  //time in ms, 5 second for example
var $input = $('#search');

// //on keyup, start the countdown
// $input.on('keyup', function (e) {

//   if (e.keyCode == 8)
//   {
    // $('.map-page .info .item').css('opacity','0.3');
    // clearTimeout(typingTimer);
    // typingTimer = setTimeout(doneTyping, doneTypingInterval);
//   } else {
//     return false;
//   }
  
// });

//on keydown, clear the countdown 
// $input.on('keydown', function () {

//   clearTimeout(typingTimer);

// });

//user is "finished typing," do something
// function doneTyping () {
//   //do something
//   $('.map-page .info .item').css('opacity','1');
  
//   console.log('done')


//   $.each(stateList, function(key, data){

//     $(data).show()
//   });

// }

$('#search').on('keydown', function(e) {

   if(e.keyCode == 8)
   {
      $.each(stateList, function(key, data){

        $(data).show();

      });
   }


      //    console.log(stateList)
      // } 
      // var something;
      // if(e.keyCode == 8)
      // {
      //   console.log(stateList)
      //   something = stateList;
      //   $('.map-page .info .item').css('opacity','0.3');

      //   clearTimeout(typingTimer);
      //   typingTimer = setTimeout(doneTyping, doneTypingInterval)

      // }

      // function doneTyping()
      // {
      //    $.each(something, function(key, data){

      //     $(data).show()
        
      //   });
      //    console.log(stateList)
      // }
    //console.log(stateList)

});

// Reset project list and empty search field
$('#reset-list').on('click', function() {
  $("#info a").show();
  $('#search').val('');
});

function search() {

    // get the value of the search input field
    var searchString = $('#search').val().toLowerCase();
    var hidden = new Array();
    var result = myLayer.setFilter(hideSite);
        // console.log(result)
    var newresult = result._layers

    $.each(newresult, function(key, data){
      // console.log(data.feature.properties.title)
      var title = data.feature.properties.title;
      var marker_id = "#" + data.feature.properties.marker_id;
      hidden.push(marker_id);
      // $(marker_id).hide();
    });


    // save list of hidden projects

    stateList = hidden;
    // here we're simply comparing the 'state' property of each marker
    // to the search string, seeing whether the former contains the latter.

    function hideSite(feature) {
        return feature.properties.title
            .toLowerCase()
            .indexOf(searchString) === -1;
    }


}

});

// myLayer.addTo(map);