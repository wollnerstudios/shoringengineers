$.preloadImages = function() {
  for (var i = 0; i < arguments.length; i++) {
    $("<img />").attr("src", arguments[i]);
  }
}

$.preloadImages("../assets/images/mapslash.jpg");

$(document).ready(function(){
  $('.white-layer-around-it').addClass('revealmap')
  setTimeout(function(){
    $('.white-layer-around-it').hide()
  },800)
  $('.launch-map-button').click(function(){
    $('.map-splash').fadeOut(600)
    $('.hide-map').fadeTo("slow",1)
  });
});


L.mapbox.accessToken = 'pk.eyJ1Ijoid29sbG5lcnN0dWRpb3MiLCJhIjoiY2lmOTE5bm11MGdobHN1a252eHQyN3RhYSJ9.ReTWRQd_NEgYvYdKLkiXoA';
var map = L.mapbox.map('map', null, {zoomControl: false, maxZoom: 18})
    .setView([34, -118], 10);

var layer_styles = {
    // Streets: L.mapbox.tileLayer('mapbox.streets'),
    // Light: L.mapbox.tileLayer('mapbox.light'),
    Satellite: L.mapbox.tileLayer('mapbox.streets-satellite'),
    // Pencil: L.mapbox.tileLayer('mapbox.pencil'),
    Shoring: L.mapbox.tileLayer('wollnerstudios.02f6f0d2'),
};

var layers = document.getElementById('menu-ui');

addLayer(L.mapbox.tileLayer('wollnerstudios.02f6f0d2'), 'Schematic', 2);

// start loading screen
// startLoading();


// addLayer(L.mapbox.tileLayer('mapbox.streets-satellite'), 'Satellite', 1);

function addLayer(layer, name, zIndex) {
  // layer
  //   .setZIndex(zIndex)
  //   .addTo(map);

    var link = document.createElement('a');
        link.href = '#';
        // link.className = 'active';
        link.innerHTML = name;
    link.onclick = function(e) {
      e.preventDefault();
      e.stopPropagation();

      if(map.hasLayer(layer)) {
        map.removeLayer(layer);
        map.addLayer(layer_styles['Satellite']);
        this.className = '';
      } else {
        map.removeLayer(layer_styles['Satellite']);
        map.addLayer(layer);
        this.className = 'active';
      }
    };
  layers.appendChild(link);
}

// var overlays = {
//   "Roads": roadsLayer
// }
// layers.Shoring.addTo(map);
//layers.Streets.addTo(map);
// layers.Streets.setZIndex(2).addTo(map);
// L.control.layers(layers).addTo(map);


new L.Control.Zoom({ position: 'bottomleft' }).addTo(map);    

// load map points
// var myLayer = omnivore.csv("/custom/joblist_full.csv", null, L.mapbox.featureLayer().addTo(map).on('error', function(error){
//   console.log(error);
// }));
console.log(map);


var myLayer = omnivore.csv("../assets/custom/joblist_full.csv", null, L.mapbox.featureLayer());


var info = document.getElementById('info');



// Iterate through each feature layer item, build a
// marker menu item and enable a click event that pans to + opens
// a marker that's associated to the marker item.

myLayer.on('ready', function (e) {
  // var searchString = $('#search').val().toLowerCase();

  var clusterGroup = new L.MarkerClusterGroup({
    polygonOptions: { weight: 2, color: '#006f51', opacity: 0.2 }
  });
  // console.log(geojson);
  e.target.eachLayer(function (layer) {
    var str = layer.feature.properties.title;

      layer.setIcon(L.mapbox.marker.icon({
        'marker-color' : '#006f51'
      }));
      clusterGroup.addLayer(layer);
      
      var mylink = info.appendChild(document.createElement('a'));
      mylink.className = 'item';
      mylink.href = '#';
      mylink.id = layer.feature.properties.marker_id;
      
      var imagetext = (layer.feature.properties.image)?'<div class="project-list-image"><img src="assets/images/map/'+layer.feature.properties.image+'"/></div>':'';
      var popupimagetext = (layer.feature.properties.image)?'<div class="project-list-image"><img src="assets/images/map/'+layer.feature.properties.image+'" /></div>':'';

      var popuptext = 
        layer.feature.properties.title +
        popupimagetext +
        '<small>' + 
        layer.feature.properties.description.toLowerCase() + 
        '<br/>' +
        layer.feature.properties.Customer.toLowerCase() +
        '</small>';
        layer.bindPopup(popuptext, {keepInView: true});

      
      mylink.innerHTML = 
        layer.feature.properties.title +
        imagetext +
        '<small>' + 
        layer.feature.properties.description.toLowerCase() + 
        '<br/>' +
        layer.feature.properties.Customer.toLowerCase() +
        '</small>';
      

      mylink.onclick = function() {
        if (/active/.test(this.className)) {
          this.className = this.className.replace(/active/, '').replace(/\s\s*$/, '');
        } else {
          var siblings = info.getElementsByTagName('a');
          for (var i = 0; i < siblings.length; i++) {
            siblings[i].className = siblings[i].className
              .replace(/active/, '').replace(/\s\s*$/, '');
          };
          this.className += ' active';
   
          map.panTo(layer.getLatLng());
          map.setZoom(18);
          map.panTo(layer.getLatLng());
          layer.openPopup();


        }
        return false;
      };

      layer.on('click', function(e){
        // alert(layer.feature.properties.marker_id);
        if (/active/.test(mylink.className)) {
          mylink.className = mylink.className.replace(/active/, '').replace(/\s\s*$/, '');
        } else {
          var siblings = info.getElementsByTagName('a');
          for (var i = 0; i < siblings.length; i++) {
            siblings[i].className = siblings[i].className
              .replace(/active/, '').replace(/\s\s*$/, '');
          };
          mylink.className += ' active';

          map.panTo(layer.getLatLng());
          map.setZoom(18);
          map.panTo(layer.getLatLng());
          layer.openPopup();
        }

        // Scroll selected item to top of list and reveal
        $('a.item.active').show();
        // $('a.item.active').slideDown(300);
        var linktop = $('a.item.active').offset().top;
        var scrollpos = $('#info').scrollTop();
        $('#info').animate({
          scrollTop: scrollpos+linktop-40
        }, 500);
        // end scroll

        return false;    

    }); 

    // }

  });// end layer each
  map.addLayer(layer_styles['Satellite']);
  map.addLayer(clusterGroup);

// list of hidden projects (state of list just before each search function call)

var stateList = [];




$('#search').keyup(function(e){
  // build list of projects to hide called stateList
  search();

  // compare each item to stateList
  $("#info a").each(function(){
    // if it's in stateList, hide it.
    if($.inArray('#'+this.id,stateList) !== -1){
      $(this).hide();
      // $(this).slideUp(300);
    } 
    // if it's not, show it
    else if($.inArray('#'+this.id,stateList) === -1) {
      $(this).show();
      // $(this).slideDown(300);
    }
  });
  
  


    
});


// Reset project list and empty search field
$('#reset-list').on('click', function() {
  $("#info a").show().removeClass('active');
  $("#info").animate({
          scrollTop: 0
        }, 500);
  $('#search').val('');
  map.setView([34, -118], 10, {
    animate: true,
  });
});

function search() {
    stateList = [];
    // get the value of the search input field
    var searchString = $('#search').val().toLowerCase();

    var hidden = new Array();
    var result = myLayer.setFilter(hideSite);
        
    var newresult = result._layers

    $.each(newresult, function(key, data){
      // console.log(data.feature.properties.title)
      var title = data.feature.properties.title;
      var marker_id = "#" + data.feature.properties.marker_id;
      hidden.push(marker_id);


    });


    // save list of hidden projects

    stateList = hidden;
    // here we're simply comparing the 'title' property of each marker
    // to the search string, seeing whether the former contains the latter.

    function hideSite(feature) {
        // Searches in title, description (address) and customer fields for a match.  
        // If it doesn't find any, it hides the project.

        return ((feature.properties.title
            .toLowerCase()
            .indexOf(searchString) === -1) &&
            (feature.properties.description
              .toLowerCase()
              .indexOf(searchString) === -1) &&
            (feature.properties.Customer
              .toLowerCase()
              .indexOf(searchString) === -1)
        );
    }


}

});


// Loading screen functions
function startLoading() {
    loader.className = '';
}

function finishedLoading() {
    // first, toggle the class 'done', which makes the loading screen
    // fade out
    loader.className = 'done';
    setTimeout(function() {
        // then, after a half-second, add the class 'hide', which hides
        // it completely and ensures that the user can interact with the
        // map again.
        loader.className = 'hide';
    }, 500);
}
// myLayer.addTo(map);