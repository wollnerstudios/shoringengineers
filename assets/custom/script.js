$.fn.redraw = function(){
  $(this).each(function(){
    var redraw = this.offsetHeight;
  });
};
function getScrollBarWidth () {
  var inner = document.createElement('p');
  inner.style.width = "100%";
  inner.style.height = "200px";
  var outer = document.createElement('div');
  outer.style.position = "absolute";
  outer.style.top = "0px";
  outer.style.left = "0px";
  outer.style.visibility = "hidden";
  outer.style.width = "200px";
  outer.style.height = "150px";
  outer.style.overflow = "hidden";
  outer.appendChild (inner);
  document.body.appendChild (outer);
  var w1 = inner.offsetWidth;
  outer.style.overflow = 'scroll';
  var w2 = inner.offsetWidth;
  if (w1 == w2) w2 = outer.clientWidth;
  document.body.removeChild (outer);
  return (w1 - w2);
};

function outerContainer()
{

	function unWow()
	{
		$('.unwow').each(function(){

			var delay = $(this).attr('data-unwow-delay');
			var animation = $(this).attr('data-unwow-animation');
			$(this).css('animation-name', animation);
			$(this).css('animation-delay', delay);
			$(this).fadeTo(delay,0);
			$(this).addClass('animated');
		});
	}
	$('#main-nav .historical, #main-header .historical').click(function(){
		closeGreenNavOverlay();
		closeGreenNavOverlayMobile();
		var url = $(this).attr('href');
		var windowurl = window.location.href;
		console.log(url)
		console.log(windowurl)
		if (url == windowurl)
		{
			return false
		} else{
			unWow();
		}
		
	})
	$('#main-nav-white a').click(function(){
		$('.brand-box').addClass('show-green-logo');
		unWowWhiteNav();

	});	
}

function innerContainer()
{
	slickSliderGallery()
		function unWows()
	{
		$('.unwow').each(function(){

			var delay = $(this).attr('data-unwow-delay');
			var animation = $(this).attr('data-unwow-animation');
			$(this).css('animation-name', animation);
			$(this).css('animation-delay', delay);
			$(this).fadeTo(delay,0);
			$(this).addClass('animated');
		});
	}
$('.side-menu .historical').click(function(){
		closeGreenNavOverlay();
		var url = $(this).attr('href');
		var windowurl = window.location.href;
		console.log(url)
		console.log(windowurl)
		if (url == windowurl)
		{
			return false
		} else{
			unWows();
		}
		
	});	
	mainSlider();
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	});



	$(".fancybox-map").fancybox({
		maxWidth: 770,
		padding:0,
    	openEffect	: 'fade',
    	closeEffect	: 'fade',
    	openSpeed	: 500,
    	closeSpeed	: 500,  		
		tpl: {
			closeBtn: '<a title="Close" class="fancybox-item fancybox-close map-popup-close-button" href="javascript:;"><i class="fa fa-close"></i></a>'
		},
	});	
	$(".fancybox").fancybox({
		maxWidth: 900,
		padding:0,
    	openEffect	: 'fade',
    	closeEffect	: 'fade',
    	openSpeed	: 500,
		scrolling   : 'no',

    	closeSpeed	: 500,  		
		tpl: {
			closeBtn: '<a title="Close" class="fancybox-item fancybox-close management-popup-close-button" href="javascript:;"><i class="fa fa-close"></i></a>'
		},
	});

    fiftiethGallery();
	new WOW().init();
	$('.see-timeline-button').click(function(){
		$('.white-cover-50').fadeOut(0);
		$('.fifty-splash-section').fadeOut(1000);
	})
}
function slickSliderGallery()
{
	$('.slick-slider-gallery').slick({
		dots: true,
		infinite: false,
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
	});
}
function setPageDelay(that, thatdelay){
	var delay = that.attr('data-historical-delay');

	setTimeout(function(){
		$('.historical').attr('data-historical-delay', thatdelay)	
	},delay)
};

$(document).ready(function(){ 
function stripTrailingSlash(str) {
    if(str.substr(-1) === '/') {
        return str.substr(0, str.length - 1);
    }
    return str;
}
	outerContainer();
  	$('#historical-home').click(function(){
  		setPageDelay($(this),0);
  	})
	$('#historical-about,.twenty-one-hundred,#historical-expertise,#historical-expertise-white, #historical-safety, #historical-contact, #historical-about-management, #historical-about-community, #historical-about-careers').click(function(){
		setPageDelay($(this),2100);
	});
	$('#historical-50th').click(function(){
		setPageDelay($(this),2000);
	});
	$('#historical-gallery').click(function(){
		setPageDelay($(this),1700);
	});  
	$('#historical-map').click(function(){
		var delay = $(this).attr('data-historical-delay');
		var url = $(this).attr('href')
		setTimeout(function(){
			window.location.href = url;
		},delay)
		return false;
	});  
	$('.map-page #main-nav a').click(function(){
		var url = $(this).attr('href')
		setTimeout(function(){
			window.location.href = url;
		},700)
		return false;	
	})
if ($(window).width() < 768) {
	
}
else {
	 $('.historical').historical({

  	navSelector: '.historical',
    container: '.historical-container',
    itemSelector:'.historical-container',
    innerHTML: true,

  	documentReady: function(){

  		innerContainer();
  		gallery();
  		$('.date-picker').datepicker({});
  		$.material.init();
  	},
    beforeReplace: function(data){
      //$('.historical-container').fadeTo(250,0);
      //console.log(data)
      $('#main-nav').find('.inner-expanded').slideUp()
      $('.active-nav').removeClass('active-nav');
    },
    afterReplace: function(data){
      window.scrollTo(0, 0);
		if (window.location.origin == stripTrailingSlash(window.location.href))
		{
			$('.brand-box').removeClass('show-green-logo');
			$('html').removeClass('secondary-page').addClass('index-page')
			wowWhiteNav();
		}	  
		if (window.location.origin != stripTrailingSlash(window.location.href))
		{
			$('.brand-box').addClass('show-green-logo');
			unWowWhiteNav();
		}		
      //$('.historical-container').fadeTo(250,1);

    },
    onPopChange: function(data){
		console.log('pop');
		if (window.location.origin != stripTrailingSlash(window.location.href))
		{

			$('.brand-box').addClass('show-green-logo');
			unWowWhiteNav();
		}
		if (window.location.origin == stripTrailingSlash(window.location.href))
		{

			$('.brand-box').removeClass('show-green-logo');
			$('html').removeClass('secondary-page').addClass('index-page')
			wowWhiteNav()			
		}
    }
  });
}
	
 
  

});


// $('.unwow').each(function(){
// 	console.log('sup')
// 	// var delay = $(this).attr('data-unwow-delay');
// 	// var animation = $(this).attr('data-unwow-animation');
// 	// $(this).attr('style','');
// 	// $(this).css('animation-name', animation);
// 	// $(this).css('animation-delay', delay);
// 	// $(this).fadeTo(delay,0);
// 	// $(this).addClass('animated');
// });



$('.toggle-image-nav').click(function(){
	// $('.brand-box').toggleClass('show-green-logo');
	// $('.brand-box .white-logo').fadeToggle();
});

$('#main-nav li').click(function(){



	if ($(this).hasClass('active-nav'))
	{
		$(this).removeClass('active-nav').find('.inner-expanded').slideUp();
	} else {
		$(this).addClass('active-nav').siblings().removeClass('active-nav').find('.inner-expanded').slideUp();
		$(this).find('.inner-expanded').slideDown();		
	}

});

var m = new Marka('#icon');
m.set('bars').color('#fff').size('50');

var mobilem = new Marka('#icon-mobile');
mobilem.set('bars').color('#fff').size('50');
function wowWhiteNav()
{
	$('#main-nav-white .unwow').each(function(){
		var delay = $(this).attr('data-wow-delay');
		var animation = $(this).attr('data-wow-animation');
		$(this).css('animation-name', animation);
		$(this).css('animation-delay', delay);
		$(this).fadeTo(delay,1);
		$(this).addClass('animated');
	});
}

function unWowWhiteNav()
{
	$('#main-nav-white .unwow').each(function(){
		var delay = $(this).attr('data-unwow-delay');
		var animation = $(this).attr('data-unwow-animation');
		$(this).css('animation-name', animation);
		$(this).css('animation-delay', delay);
		$(this).fadeTo(delay,0);
		$(this).addClass('animated');
	});
}
function closeGreenNavOverlay()
{
    m.set('bars');
	$('body').css('padding-right','');
    $('#menu-overlay').removeClass('open');
	$('html').removeClass('menu-open');
    $('#main-nav .container').removeClass('animated fadeInDown').addClass('animated fadeOutUp')
      setTimeout(function(){
       $('#main-nav').removeClass('open');
    },700)	
}

function openGreenNavOverlay()
{
    var scrollBarWidth = getScrollBarWidth();
	m.set('times');
    $('#menu-overlay').addClass('open');
	$('html').addClass('menu-open');
    $('#main-nav').addClass('open');
	$('body').css('padding-right',scrollBarWidth);
    $('#main-nav .container').removeClass('animated fadeOutUp').addClass('animated fadeInDown open');
}


function openGreenNavOverlayMobile()
 {
	var scrollBarWidth = getScrollBarWidth();
	mobilem.set('times');
	$('#menu-overlay').addClass('open');
	$('html').addClass('menu-open');
	$('#main-nav').addClass('open');
	$('body').css('padding-right',scrollBarWidth);
	$('#main-nav .container').removeClass('animated fadeOutUp').addClass('animated fadeInDown open');
}
function closeGreenNavOverlayMobile()
{
    mobilem.set('bars');
	$('body').css('padding-right','');
    $('#menu-overlay').removeClass('open');
	$('html').removeClass('menu-open');
    $('#main-nav .container').removeClass('animated fadeInDown').addClass('animated fadeOutUp')
      setTimeout(function(){
       $('#main-nav').removeClass('open');
    },700)	
}

$('#mobile-header .icon-section').click(function(){
  var state = $(this).children('#icon-mobile').attr('data-icon');

  if (state == 'bars')
  {
  	openGreenNavOverlayMobile();
  } else {
  	closeGreenNavOverlayMobile();
  }

 	

});

$('.green-nav-box').click(function(){
  var state = $(this).children('#icon').attr('data-icon');  
  if (state == 'bars')
  {
  	openGreenNavOverlay();
  } else {
  	closeGreenNavOverlay();
  }
});

function gallery()
{
	$('.fancybox-gallery').fancybox({
    	openEffect	: 'fade',
    	closeEffect	: 'fade',
    	openSpeed	: 500,
    	closeSpeed	: 500,
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}    	  
	});
	// $('.shoring-gallery').mixItUp({
	// 	animation:{
	// 		duration:1000,
	// 		effects: 'fade stagger'
	// 	},
	// 	callbacks: {
	// 		onMixLoad: function(state){
	// 			setTimeout(function(){
	// 				$('#footer').fadeTo("slow",1)
	// 			},10)
				
	// 		}
	// 	}		
	// });

	// $('.mix').magnificPopup({
	// 	type: 'image',
	// 	closeOnContentClick: true,
	// 	closeBtnInside: false,
	// 	fixedContentPos: true,
	// 	mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
	// 	image: {
	// 		verticalFit: true
	// 	},
	// 	zoom: {
	// 		enabled: true,
	// 		duration: 500 // don't foget to change the duration also in CSS
	// 	}
	// });



};


function mainSlider()
{
	$("#slider1").revolution({
		sliderType:"standard",
		sliderLayout:"auto",
		delay:9000,
		navigation: {
			arrows:{enable:false}       
		},      
		gridwidth:1280,
		gridheight:803    
	});	
}

 	
function fiftiethGallery()
{
	var $gallery = $('#gallerytest'),
		$slide = $('#gallerytest .fiftieth-slide'),
		$backButton = $('.back-button-50'),
		$copy = $('.text-copy-block'),		
		$smallCopy = $('.smallish'),
		$largeCopy = $('.largish'),
		$brandBox = $('.brand-box'),
		$copyImg = $('.copy-img')
	
	$gallery.addClass('initializing');
	setTimeout(function(){
		$gallery.removeClass('initializing');
	},1500)

	// $gallery.mouseenter(function(){
	// 	$(this).addClass('hovering')
	// }).mouseleave(function(){
	// 	$(this).removeClass('hovering')
	// });

	$slide.hover(function(){
		$(this).addClass('active').siblings().removeClass('active');
	});

	$slide.click(function(){
		$(this).addClass('active').siblings().removeClass('active');
		$gallery.removeClass('tabled');
		$brandBox.css('opacity',0);
		$brandBox.css('visibility','hidden');
		$brandBox.css('visibility','hidden');
		$brandBox.css('pointer-events','none');
		changeCopy($(this));
		setTimeout(function(){
			$gallery.addClass('open')
		},10);
		setTimeout(function(){
			$copy.removeClass('invisible fadeOutDown').addClass(' fadeInUp');
		},300);
		setTimeout(function(){
			$gallery.addClass('open-completed')
		},1500);
	});
	$backButton.click(function(){
		$copy.removeClass('fadeInUp').addClass('fadeOutDown');
		$gallery.addClass('closing');
		setTimeout(function(){
			$brandBox.css('opacity',1);
			$brandBox.css('visibility','visible')
			$brandBox.css('pointer-events','');
		},2300);
		
		setTimeout(function(){
			$gallery.removeClass('closing');
		},2300);
		setTimeout(function(){
			$gallery.removeClass('open open-completed');
		},700);
		setTimeout(function(){
			$gallery.addClass('tabled');
		},2300)
	});
	function changeCopy($slide)
	{
		var smallCopy = $slide.attr('data-slide-smallcopy'),
			largeCopy = $slide.attr('data-slide-largeCopy'),
			imgFile = $slide.attr('data-slide-imgFile');
		$copyImg.attr('src', imgFile);
		$smallCopy.text(smallCopy);
		$largeCopy.text(largeCopy);
		return;
	} 		
}

function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = escape(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}



/* jQuery(document).ready(function($) {

	var mapsplash = readCookie('mapsplash');
	
	console.log(mapsplash)
	
	if (mapsplash.length)
	{
		
		
		
	} else {
		
		
		
	}
	
	$('.launch-map-button').click(function(){
		
	})
});
 */

/* $('#full-site').click(function() {
    createCookie('viewport', 'desktop', 1);
    var viewportCookie = readCookie('viewport');
    console.log(viewportCookie);
    $('meta[name=viewport]').attr("content", "")
});
$('#mobile-site').click(function() {
    createCookie('viewport', 'mobile', 1);
    $('meta[name=viewport]').attr("content", "width=device-width, initial-scale=1")
})

 */