
L.mapbox.accessToken = 'pk.eyJ1Ijoid29sbG5lcnN0dWRpb3MiLCJhIjoiY2lmOTE5bm11MGdobHN1a252eHQyN3RhYSJ9.ReTWRQd_NEgYvYdKLkiXoA';
var map = L.mapbox.map('map', 'mapbox.light', {zoomControl: false})
    .setView([34, -118], 10)
new L.Control.Zoom({ position: 'bottomleft' }).addTo(map);    

// load map points
// var myLayer = omnivore.csv("/custom/joblist_full.csv", null, L.mapbox.featureLayer().addTo(map).on('error', function(error){
//   console.log(error);
// }));

var myLayer = omnivore.csv("/custom/joblist_full.csv", null, L.mapbox.featureLayer());

var info = document.getElementById('info');



// Iterate through each feature layer item, build a
// marker menu item and enable a click event that pans to + opens
// a marker that's associated to the marker item.

myLayer.on('ready', function (e) {
  var clusterGroup = new L.MarkerClusterGroup({
    polygonOptions: { weight: 2, color: '#006f51', opacity: 0.2 }
  });
  // console.log(geojson);
  e.target.eachLayer(function (layer) {
    layer.setIcon(L.mapbox.marker.icon({
      'marker-color' : '#006f51'
    }));
    clusterGroup.addLayer(layer);
    
    var mylink = info.appendChild(document.createElement('a'));
    mylink.className = 'item';
    mylink.href = '#';
    mylink.id = layer.feature.properties.marker_id;
    
    var imagetext = (layer.feature.properties.image)?'<div class="project-list-image"><img src="/images/map/'+layer.feature.properties.image+'"/></div>':'';
    var popupimagetext = (layer.feature.properties.image)?'<div class="project-list-image"><img src="/images/map/'+layer.feature.properties.image+'" /></div>':'';

    var popuptext = 
      layer.feature.properties.title +
      popupimagetext +
      '<small>' + 
      layer.feature.properties.description.toLowerCase() + 
      '<br/>' +
      layer.feature.properties.Customer.toLowerCase() +
      '</small>';
      layer.bindPopup(popuptext, {keepInView: true});

    
    mylink.innerHTML = 
      layer.feature.properties.title +
      imagetext +
      '<small>' + 
      layer.feature.properties.description.toLowerCase() + 
      '<br/>' +
      layer.feature.properties.Customer.toLowerCase() +
      '</small>';
    

    mylink.onclick = function() {
      if (/active/.test(this.className)) {
        this.className = this.className.replace(/active/, '').replace(/\s\s*$/, '');
      } else {
        var siblings = info.getElementsByTagName('a');
        for (var i = 0; i < siblings.length; i++) {
          siblings[i].className = siblings[i].className
            .replace(/active/, '').replace(/\s\s*$/, '');
        };
        this.className += ' active';
 
        map.panTo(layer.getLatLng());
        map.setZoom(19);
        map.panTo(layer.getLatLng());
        layer.openPopup();


      }
      return false;
    };

    layer.on('click', function(e){
      // alert(layer.feature.properties.marker_id);
      if (/active/.test(mylink.className)) {
        mylink.className = mylink.className.replace(/active/, '').replace(/\s\s*$/, '');
      } else {
        var siblings = info.getElementsByTagName('a');
        for (var i = 0; i < siblings.length; i++) {
          siblings[i].className = siblings[i].className
            .replace(/active/, '').replace(/\s\s*$/, '');
        };
        mylink.className += ' active';
        // map.setZoom(13);
        map.panTo(layer.getLatLng());
        // layer.openPopup();
      }

      // Scroll selected item to top of list
      var linktop = $('a.item.active').offset().top;
      var scrollpos = $('#info').scrollTop();
      $('#info').animate({
        scrollTop: scrollpos+linktop
      }, 500);
      // end scroll

      return false;      
    });



  });
  map.addLayer(clusterGroup);



});
$('#search').keyup(search);

// var csvLayer = omnivore.csv('/mapbox.js/assets/data/airports.csv', null, L.mapbox.featureLayer())
//     .addTo(map);

function search() {
    // get the value of the search input field
    var searchString = $('#search').val().toLowerCase();

    myLayer.setFilter(showState);
    console.log(myLayer)
    // here we're simply comparing the 'state' property of each marker
    // to the search string, seeing whether the former contains the latter.
    function showState(feature) {
        return feature.properties.title
            .toLowerCase()
            .indexOf(searchString) !== -1;
    }
}
// myLayer.addTo(map);