if ($(window).width() < 768) {
    donothing();
}
else {
    initHistorical();
}
function donothing() {

}
function initHistorical() {
    (function ($) {

        $.fn.historical = function (options) {

            var url,
                data = [],
                responseData,
                container,
                initialState,
                itemSelector,
                innerHTML,
                replaceDelay,
                navSelector;

            $.fn.historical.defaults = {

                innerHTML: true,
                navSelector: '.historical',
                itemSelector: '.historical-container',
                container: '.historical-container',
                documentReady: function () {},
                beforeReplace: function () {},
                afterReplace: function () {},
                onPopChange: function () {}
            };

            var opts = $.extend({}, $.fn.historical.defaults, options);
            itemSelector = opts.itemSelector;
            navSelector = opts.navSelector;
            container = opts.container;
            innerHTML = opts.innerHTML;
            initialState = $(container).html();

            opts.documentReady.call(this);

            var stripTrailingSlash = function (str) {
                if (str.substr(-1) === '/') {
                    return str.substr(0, str.length - 1);
                }
                return str;
            };

            var handleClick = function (hashedURL) {

                data = [];

                url = $(this).attr('href');

                replaceDelay = $('.historical-timing').attr('data-historical-delay');

                if (typeof replaceDelay === 'undefined') {
                    replaceDelay = 0;
                }

                if (url == stripTrailingSlash(window.location.toString())) {
                    return false;
                }

                getDatas('https://shoringengineers.com/pages/' + url.replace('https://shoringengineers.com/', '') + '.php', itemSelector, function (responseData) {

                    $.each(responseData, function (key, value) {
                        if (innerHTML) {
                            data.push(value.innerHTML);

                        } else {

                            data.push(value.outerHTML);

                        }

                    });

                    opts.beforeReplace.call(this, data);

                    history.pushState(data, null, url);

                    setTimeout(function () {

                        $(container).html(data);
                        opts.afterReplace(data);
                        opts.documentReady.call(this)

                    }, replaceDelay)
                });

                return false;

            };

            $(navSelector).on('click', (
                handleClick
            ));

            $(container).on('click', navSelector, (
                handleClick
            ));

            $.ajaxSettings.beforeSend=function(xhr){
                xhr.setRequestHeader('X-Requested-With', {toString: function(){ return ''; }});
            };

            function getDatas(url, dom, callback) {
                return $.ajax({

                    url: url,
                    type: 'GET',
                    success: function (responseData) {
                        var $source = $('<div>' + responseData + '</div>');

                        callback($source.find(dom));

                    },
                });
            }

            var popped = ('state' in window.history), initialURL = location.href;

            $(window).bind("popstate", function (e) {

                if (window.location.hash) {

                    return false;
                }

                var initialPop = !popped && location.href == initialURL;

                popped = true;

                if (initialPop) return;

                popData = e.originalEvent.state;

                if (popData == null) {
                    $(container).html(initialState);
                    opts.onPopChange(popData);
                    opts.documentReady.call(this);

                } else {

                    $(container).html(popData);
                    opts.onPopChange(popData);
                    opts.documentReady.call(this);
                }
            });
        }

    }(jQuery));

}
