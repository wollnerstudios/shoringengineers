<?php require 'config/config.php'; ?>
<!DOCTYPE html>
<html lang="en" class="map-page secondary-page">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Shoring Engineers is over 40 years strong - in a field where strength is everything. We are one of the oldest shoring contractors in California."/>
        <title>Project Map </title>
        <meta name="keywords" content="keyword "/>
        <meta name="author" content="WollnerStudios, Inc">
        <meta name="robots" content="all"/>
        <meta name="robots" content="index, follow"/>
        <meta name="revisit-after" content="4 days"/>
        <link rel="icon" href="https://shoringengineers.com/favicon.ico">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href="<?php echo VENDORPATH.'bootstrap/dist/css/bootstrap.min.css'; ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo VENDORPATH.'wow/css/libs/animate.css'; ?>">
        <link rel="stylesheet" href="<?php echo VENDORPATH.'font-awesome/css/font-awesome.css'; ?>">
        <link rel="stylesheet" href="<?php echo VENDORPATH.'marka/dist/css/marka.min.css'; ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo VENDORPATH.'revolution/css/settings.css'; ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo VENDORPATH.'revolution/css/layers.css'; ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo VENDORPATH.'revolution/css/navigation.css'; ?>">

        <link href='https://api.mapbox.com/mapbox.js/v2.2.2/mapbox.css' rel='stylesheet'/>
        <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css' rel='stylesheet'/>
        <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css' rel='stylesheet'/>

        <link rel="stylesheet" href="<?php echo VENDORPATH.'magnific/dist/magnific-popup.css'; ?>">
        <link rel="stylesheet" href="<?php echo VENDORPATH.'fancybox/source/jquery.fancybox.css?v=2.1.5'; ?>" type="text/css" media="screen"/>
        <link rel="stylesheet" href="<?php echo VENDORPATH.'fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7'; ?>" type="text/css" media="screen"/>
        <link rel="stylesheet" href="<?php echo CUSTOMPATH.'material-form.css'; ?>">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css"/>

        <link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom-form.css'; ?>">
        <link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom.css'; ?>">
        <link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom-md.css'; ?>">
        <link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom-sm.css'; ?>">
        <link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom-xs.css'; ?>">
        <link rel="stylesheet" href="<?php echo CUSTOMPATH.'mapbox.css'; ?>">
        <link rel="stylesheet" href="<?php echo CUSTOMPATH.'management.css'; ?>">
        <link rel="stylesheet" href="<?php echo CUSTOMPATH.'testimonials.css'; ?>">
        <link rel="stylesheet" href="<?php echo CUSTOMPATH.'subpages.css'; ?>">
        <link rel="stylesheet" href="<?php echo CUSTOMPATH.'custom-style.css'; ?>">
        <link rel="stylesheet" href="<?php echo VENDORPATH.'lazyloadxt/jquery.lazyloadxt.spinner.css'; ?>">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="page-wrap">
            <div class="map-splash">
                <div class="container">
                    <div class="row">
                        <div class="pad-b-2 col-xs-12 col-sm-6 col-sm-push-6 map-splash-copy ">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>
                                        Shoring Engineers is 50 years strong&#8212;in a field where strength is everything. We are one of the oldest shoring contractors in California&#8212;and one of the only companies in Southern California that performs shoring, excavation and shotcrete applications together as a team.
                                    </p>

                                </div>
                                <div class="col-xs-12 text-center">
                                    <div class="launch-map-button">
                                        Launch Map
                                    </div>
                                    <div class="col-xs-12 text-center">
                                        <a href="#map-popup" class="fancybox-map launch-instructions-button">
                                            HOW THE MAP WORKS (instructions)
                                        </a>
                                        <div id="map-popup">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-xs-4 pad-r-0 pad-l-0">
                                                        <img class="width-100" src="<?php echo 'https://shoringengineers.com/assets/images/'.'map-splash-instructons.jpg'; ?>" alt="">
                                                    </div>
                                                    <div class="col-xs-8">
                                                        <div class="row">
                                                            <div class="col-xs-12 title-part">
                                                                <h2>
                                                                    Map Instructions
                                                                </h2>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <ul>
                                                                    <li>Circled numbers indicate projects within the region.</li>
                                                                    <li>Zoom in and out with the + &amp; - symbols in the lower left.</li>
                                                                    <li>Search for a specific project, construction company, or building corporation in the Filter Project Sites bar top right.</li>
                                                                    <li>Select a specific site within the right column listing.</li>
                                                                    <li>Switch from rendering view to satellite view in the lower right.</li>
                                                                    <li>Reset button on bottom right to reset map and column listing.</li>
                                                                    <li>Zoom out to return to circled numbered map.</li>
                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-6 col-sm-pull-6 map-image-container">
                            <img class="img-responsive" src="<?php echo 'https://shoringengineers.com/assets/images/'.'mapsplashcopy.png'; ?>" alt="">
                        </div>
                    </div>
                </div>

            </div>

            <header id="main-header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="brand-box show-green-logo">
                                <div class="green-nav">
                                    <div class="green-nav-logo">
                                        <a href="<?php echo 'https://shoringengineers.com/'; ?>">
                                            <img class="toggle-image-nav img-responsive green-logo" src="<?php echo 'https://shoringengineers.com/assets/images/'.'green-alpha-logo.png'; ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="green-nav-box">
                                        <i id="icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </header>
            <header id="mobile-header" class="" style="display:none;">
                <div class="table-fluid">
                    <div class="table-row text-center">
                        <div class="table-cell col-xs-3 brand-section">
                            <a href="<?php echo 'https://shoringengineers.com/'; ?>" class="historical">
                                <img src="<?php echo 'https://shoringengineers.com/assets/images/'.'green-alpha-logo.png'; ?>" alt="">
                            </a>
                        </div>
                        <div class="table-cell col-xs-3 icon-section ">
                            <i id="icon-mobile"></i>
                        </div>
                        <div class="table-cell col-xs-3 phone-section">
                            <a href="tel:+1-562-944-9331">
                                <i class="fa fa-phone"></i>
                            </a>
                        </div>
                        <div class="table-cell col-xs-3 contact-section">
                            <a href="<?php echo 'https://shoringengineers.com/'.'contact'; ?>" class="historical">
                                <i class="fa fa-envelope"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </header>
            <nav class="" id="main-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <ul>
                                <li>
                                    <div class="large-font has-submenu">areas of expertise
                                        <i class="fa fa-angle-double-right"></i>
                                        <ul class="inner-expanded">
                                            <li>
                                                <a href="<?php echo 'https://shoringengineers.com/'.'expertise/project-management'; ?>">project management</a>
                                            </li>
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'expertise/markets'; ?>">markets</a></li>
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'expertise/shoring'; ?>">shoring</a></li>
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'expertise/excavation'; ?>">excavation</a>
                                            </li>
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'expertise/caissons'; ?>">caissons</a></li>
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'expertise/shotcrete'; ?>">shotcrete</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="large-font has-submenu">about shoring
                                        <i class="fa fa-angle-double-right"></i>
                                        <ul class="inner-expanded">
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'about/history'; ?>">history</a></li>
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'about/mission'; ?>">mission</a></li>
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'about/management'; ?>">management</a></li>
                                            <li>
                                                <a href="<?php echo 'https://shoringengineers.com/'.'about/community-involvement'; ?>">community involvement</a>
                                            </li>
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'about/testimonials'; ?>">testimonials</a>
                                            </li>
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'about/careers'; ?>">careers</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a class="large-font" href="50th">50th Anniversary</a></li>
                                <li>
                                    <div class="large-font has-submenu">resources
                                        <i class="fa fa-angle-double-right"></i>
                                        <ul class="inner-expanded">
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'resources/people'; ?>">people</a></li>
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'resources/equipment'; ?>">equipment</a>
                                            </li>
                                            <li><a href="<?php echo 'https://shoringengineers.com/'.'resources/tooling'; ?>">tooling</a></li>
                                        </ul>
                                    </div>
                                </li>

                                <li><a class="large-font" href="<?php echo 'https://shoringengineers.com/'.'gallery'; ?>">project gallery</a>
                                </li>
                                <li><a class="large-font" href="<?php echo 'https://shoringengineers.com/'.'map'; ?>">project map</a></li>
                                <li><a class="large-font" href="<?php echo 'https://shoringengineers.com/'.'safety'; ?>">safety</a></li>
                                <li><a class="large-font" href="<?php echo 'https://shoringengineers.com/'.'contact'; ?>">contact</a></li>
                                <li class="social-menu-section">
                                    <a href="https://www.facebook.com/shoringengineers/?fref=ts" target="_blank" class="mar-r-0-5"><i class="fa fa-facebook"></i></a>
                                    <a href="https://www.linkedin.com/company/shoring-engineers?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A2918649%2Cidx%3A2-3-4%2CtarId%3A1448916989750%2Ctas%3Ashorin" target="_blank"><i class="fa fa-linkedin"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="white-layer-around-it">

            </div>
            <div class="map-container hide-map">
                <div id='map' class='map'></div>
                <div class="search-block">
                    <input id='search' class='search-ui' placeholder='Filter project sites'/>
                </div>
                <div id='info' class='info'></div>
                <nav id='menu-ui' class='menu-ui'>
                    <div id="reset-list">Reset Map</div>
                </nav>
            </div>


            <footer class="pad-t-2 pad-b-2" id="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-xs-6">
                            <img class="img-responsive" src="<?php echo 'https://shoringengineers.com/assets/images/'.'Shoring-logo-bottom.png'; ?>" alt="">
                        </div>
                        <div class="col-xs-3 hidden-xs" id="footer-address-block">
                            <address>
                                Shoring Engineers <br>
                                12645 Clark Street <br>
                                Santa Fe Springs <br>
                                CA 90670 <br>
                                Phone: <a href="tel:+1-562-944-9331">(562) 944-9331</a> <br>
                                Fax: <a href="tel:+1-562-941-8098">(562) 941-8098</a>
                            </address>
                        </div>
                        <div class="col-sm-4 col-xs-6" id="footer-license-block">
                            CA Lic. #245416-A <br>
                            AZ Lic. #ROC124501<br>
                            NV Lic. #0055754 <br>
                            OR Lic. #188382 <br>
                            UT Lic. #312403-5501 <br>
                            WA Lic. #2610<br>
                            <span class="hidden-xs"><br><br></span>
                            <span class="hidden-xs">Website Development by
                                <a href="<?php echo 'https://shoringengineers.com/'; ?>">WollnerStudios.com</a></span>
                        </div>
                        <div class="visible-xs">
                            <div class="col-xs-12">
                                <address>

                                    12645 Clark Street
                                    Santa Fe Springs
                                    CA 90670 <br>
                                    Phone: <a href="tel:+1-562-944-9331">(562) 944-9331</a>
                                    Fax: <a href="tel:+1-562-941-8098">(562) 941-8098</a>
                                </address>
                            </div>
                        </div>
                        <div class="col-sm-3 hidden-xs">
                            <a target="_blank" href="https://www.linkedin.com/company/2918649?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A2918649%2Cidx%3A2-3-4%2CtarId%3A1448916989750%2Ctas%3Ashorin" class="pull-right pad-l-2">
                                <img class="img-responsive pull-right" src="<?php echo 'https://shoringengineers.com/assets/images/'.'linkedIn-icon.png'; ?>" alt="">
                            </a>
                            <a target="_blank" href="https://www.facebook.com/shoringengineers/?fref=ts" class="pull-right ">
                                <img class="img-responsive pull-right" src="<?php echo 'https://shoringengineers.com/assets/images/'.'facebook-icon.png'; ?>" alt="">
                            </a>
                        </div>
                        <div class="visible-xs mobile-social">
                            <div class="col-xs-12">
                                <a target="_blank" href="https://www.facebook.com/shoringengineers/?fref=ts" class=" ">
                                    <img class="img-responsive " src="<?php echo 'https://shoringengineers.com/assets/images/'.'facebook-icon.png'; ?>" alt="">
                                </a>
                                <a target="_blank" href="https://www.linkedin.com/company/2918649?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A2918649%2Cidx%3A2-3-4%2CtarId%3A1448916989750%2Ctas%3Ashorin" class=" pad-l-2">
                                    <img class="img-responsive " src="<?php echo 'https://shoringengineers.com/assets/images/'.'linkedIn-icon.png'; ?>" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <div id="menu-overlay"></div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="<?php echo VENDORPATH.'bootstrap/dist/js/bootstrap.min.js'; ?>"></script>
        <script src="<?php echo VENDORPATH.'marka/dist/js/marka.min.js'; ?>"></script>
        <script src="<?php echo VENDORPATH.'wow/dist/wow.min.js'; ?>"></script>
        <script src="<?php echo VENDORPATH.'revolution/js/jquery.themepunch.tools.min.js?rev=5.0'; ?>"></script>
        <script src="<?php echo VENDORPATH.'revolution/js/jquery.themepunch.revolution.min.js?rev=5.0'; ?>"></script>
        <script src='https://api.mapbox.com/mapbox.js/v2.2.2/mapbox.js'></script>
        <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-omnivore/v0.2.0/leaflet-omnivore.min.js'></script>
        <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js'></script>
        <script src="<?php echo VENDORPATH.'lazyloadxt/jquery.lazyloadxt.min.js'; ?>"></script>
        <script src="<?php echo CUSTOMPATH.'mapbox.js'; ?>"></script>
        <script src="<?php echo CUSTOMPATH.'historical.js'; ?>"></script>
        <script src="<?php echo VENDORPATH.'mixitup/mixitup.js'; ?>"></script>
        <script src="<?php echo VENDORPATH.'magnific/dist/jquery.magnific-popup.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo VENDORPATH.'fancybox/source/jquery.fancybox.pack.js?v=2.1.5'; ?>"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
        <script src="<?php echo CUSTOMPATH.'material-form.js'; ?>"></script>
        <script src="<?php echo CUSTOMPATH.'script.js'; ?>"></script>


    </body>
</html>